"""StateMachine (menu manager) and base menus definition"""
import re
import pandas as pd

from curtsies import Input
from unidecode import unidecode

from lib.rich.rich.table import Table
from lib.rich.rich.text import Text
from lib.rich.rich.style import Style
from lib.rich.rich.align import Align
from lib.rich.rich.panel import Panel

from bibrew.panels import Header, BlinkHeader, Body, Footer, TopSide


class EmptyDataFrameError(Exception):
    pass


class State:
    def __init__(self, name):
        self.name = name
        self.parent = None
        self.isdone = False
        self.next = None
        self.previous = None

    def enter(self):
        """Method to enter state"""
        self.isdone = False

    def exit(self):
        """Do-noting method to exit state"""

    def reset(self):
        """Do-nothing method to reset state"""

    def display(self):
        """Do-nothing method to display state"""

    @staticmethod
    def get_keypressed():
        """Get keys pressed"""
        with Input(keynames="curses") as input_generator:
            for key in input_generator:
                return repr(key)

    def push_next(self, state_name):
        """Pushes next state by name and sets self.next and next.previous"""
        next_state = self.parent.state_dict[state_name]
        self.next = next_state
        next_state.previous = self
        self.parent.push_state(state_name)


class StateMachine(State):
    def __init__(self, name):
        super().__init__(name)
        self.state_dict = {}
        self.last_state = None
        self.state_stack = []
        self.isdone = False

    def update(self):
        """ Update states
            note that it's sometimes beneficial to have an extra
            method on your state objects to allow them to forcibly
            seize control in certain scenarios, this simple model
            doesn't include such things
        """

        # no processing an empty stack often this will be used
        # to exit the menu or application (for example)
        if len(self.state_stack) <= 0:
            # here we're manually exiting the last state (if any)
            # since we are ensuring predictable behaviour by delaying
            # calls to enter() and exit() ie. not exiting a state
            # when it's done, but instead once we're ready for it to be
            self.exit_state(self.last_state)
            self.isdone = True
            return

        current_state = self.state_stack[-1]

        if self.last_state != current_state:
            # We've switched states, notify everyone involved
            self.switch_state(current_state)

        if current_state is not None:
            current_state.update()
            if current_state.isdone:
                # Reset state if it is done, then remove it from state_stack
                current_state.reset()
                self.pop_state()
            # Push next state if it is not None
            if current_state.next is not None:
                self.push_state(current_state.next.name)

    def reset(self):
        """ Reset the state machine, but leave its dictionary intact """
        self.state_stack = []
        self.last_state = None
        self.isdone = False

    def switch_state(self, new_state):
        """ Handles exiting and entering states appropriately """
        self.exit_state(self.last_state)
        self.enter_state(new_state)
        self.last_state = new_state

    @staticmethod
    def enter_state(state):
        """ Helper function to safely enter the specified state object """
        if state is not None:
            state.enter()

    @staticmethod
    def exit_state(state):
        """ Helper function to safely exit the specified state object """
        if state is not None:
            state.exit()

    def add_state(self, state):
        """ Add a state to the dictionary for later
            ! Doesn't push or enter !
        """
        state.parent = self
        self.state_dict[state.name] = state

    def push_state(self, state_name):
        """ Push a state onto the stack by name """
        new_state = self.state_dict[state_name]
        self.state_stack.append(new_state)

    def pop_state(self):
        """Remove the last state from the stack"""
        self.state_stack.pop()


class BaseMenu(State):
    """Base menu class. Features :
    List of options on the left
    Static display in the body"""
    # Constructor
    def __init__(self, name, itf):
        super().__init__(name)
        self.itf = itf
        self.rich_name = ""
        self.options = {}
        self.cycle_options = ["q", "\\x1b"]
        self.error_msg = None

    def update(self, key=None):
        """Method to update the state"""
        if key is None:
            self.display()
            key = self.keypress_prompt()
        if key in ["q", "\\x1b"]:
            self.next = None
            self.isdone = True
        elif key in list(self.options.keys()):
            newstate_name = self.options[key][0]
            if newstate_name is not None:
                newstate = self.parent.state_dict[newstate_name]
                self.next = newstate
            if self.options[key][3]:
                self.isdone = True
        return key

    def enter(self):
        """Enters state"""
        header = Header(self.itf)
        self.itf.layout["header"].update(header)
        self.isdone = False
        self.next = None
        self.pandas_df = self.get_pandas_df()

    def exit(self):
        if self.next is not None:
            self.next.previous = self
        header = BlinkHeader(self.itf)
        self.itf.layout["header"].update(header)
        self.itf.console.print(self.itf.layout)

    def reset(self):
        self.next = None

    def add_option(self, option_key, state_name, print_option, print_key, trigger_exit=False, select_id=False):
        """Adds an option to this Menu. An option consists of a key,
        next state name, text of the option, and text of the key.
        The last value in each key-value pair denotes whether the given option
        should trigger an exit from the current Menu after it is called."""
        self.options[option_key] = [state_name,
                                    print_option,
                                    print_key,
                                    trigger_exit,
                                    select_id]

    def size(self):
        """Returns the number of options in this Menu"""
        return len(self.options)

    def display(self):
        """Prints the Menu in its entirety"""
        base_style = self.itf.console.get_style("base")
        text_style = self.itf.console.get_style("content")
        # Body
        body = self.get_body()
        self.itf.layout["body_topleft"].update(body)
        # Topside
        options_grid = self.get_options_grid()
        quit_text = Text("", style=text_style)
        quit_text.append("q, Esc", style=text_style + Style(bold=True))
        quit_text.append(" : Quitter ce menu", style=text_style)
        options_grid.add_row(quit_text)
        self.itf.layout["topside"].update(TopSide(itf=self.itf,
                                                  renderable=options_grid,
                                                  title="Liste des commandes"))
        # Print
        self.itf.console.print(self.itf.layout, style=base_style)

    def get_options_grid(self):
        """Generates options grid"""
        text_style = self.itf.console.get_style("content")
        options_grid = Table.grid(expand=True)
        options_grid.add_column(justify="center")
        for i in range(0, self.size()):
            key = list(self.options.keys())[i]
            option = self.options[key][1]
            key_print = self.options[key][2]
            option = re.sub("^.", "", option)
            text = Text("(", style=text_style)
            text.append(key_print, style=text_style + Style(bold=True))
            text.append(")" + option, style=text_style)
            options_grid.add_row(text)
        return options_grid

    def get_body(self):
        """Function to generate body"""
        rich_table = self.itf.pandas_to_rich(self.pandas_df,
                                             rich_name=self.rich_name)
        return Body(self.itf,
                    rich_table)

    def get_pandas_df(self):
        """gets panda dataframe from sqlalchemy statement"""
        return pd.DataFrame()

    def format_df(self, pandas_df):
        """Formats pandas_df to remove id column and map columns for printing"""
        pandas_df = pandas_df.copy().\
            drop("id", axis=1, errors="ignore")
        if "step" in pandas_df.columns.tolist():
            steps_dict = self.itf.steps_df[["step", "print"]].\
                set_index('step').to_dict()
            pandas_df["step"] = pandas_df["step"].\
                apply(lambda x: steps_dict["print"][x])
        if "form" in pandas_df.columns.tolist():
            pandas_df["form"] = pandas_df["form"].\
                apply(lambda x: self.itf.yeast_forms[x])
        if "type" in pandas_df.columns.tolist():
            pandas_df["type"] = pandas_df["type"].\
                apply(lambda x: self.itf.yeast_types[x])
        if "floculation" in pandas_df.columns.tolist():
            pandas_df["floculation"] = pandas_df["floculation"].\
                apply(lambda x: self.itf.yeast_floc[x])
        return pandas_df

    def keypress_prompt(self):
        """prompt with keypresses,
        show message in footer if choice not accepted"""
        key = State.get_keypressed()
        key = re.sub("'", "", key)
        options = list(self.options.keys())
        base_style = self.itf.console.get_style("base")
        while key not in options + self.cycle_options:
            self.itf.layout["footer"].visible = True
            self.itf.layout["footer"].update(
                Footer(self.itf,
                       f"{key}, Entrez une des commandes suivantes : {options}")
            )
            self.itf.console.print(self.itf.layout, style=base_style)
            key = State.get_keypressed()
            key = re.sub("'", "", key)
        self.itf.layout["footer"].visible = False
        return key


class MultiMenu(BaseMenu):
    def __init__(self, name, itf):
        super().__init__(name, itf)
        self.pandas_dict = {}

    def enter(self):
        super().enter()
        self.pandas_dict = self.get_pandas_dict()
        self.itf.four_body()

    def exit(self):
        super().exit()
        self.itf.one_body()

    def get_panel(self, panel_name):
        """Gets rawmat table and format body"""
        table_name = self.pandas_dict[panel_name][1]
        pandas_df = self.format_df(self.pandas_dict[panel_name][0])
        table = self.itf.pandas_to_rich(pandas_df, table_name)
        panel = Body(self.itf, table)
        return panel

    def display(self):
        text_style = self.itf.console.get_style("content")
        base_style = self.itf.console.get_style("base")
        # Body
        self.get_body()
        # Options
        options_grid = super().get_options_grid()
        quit_text = Text("", style=text_style)
        quit_text.append("q, Esc", style=text_style + Style(bold=True))
        quit_text.append(" : Quitter ce menu", style=text_style)
        options_grid.add_row(quit_text)
        self.itf.layout["topside"].update(TopSide(itf=self.itf,
                                                  renderable=options_grid,
                                                  title="Liste des commandes"))
        self.itf.console.print(self.itf.layout, style=base_style)


class SelectMenu(BaseMenu):
    """Base class for selection menus. Features :
    Displays options on the left
    Filter the returned table based on user input,
    by default on the name column.
    """

    def __init__(self, name, itf):
        super().__init__(name, itf)
        self.add_option("f",
                        None,
                        "Filtrer\n\
    - Entrée : valider le filtre\n\
    - Esc : annuler\n",
                        "f")
        self.select_options_list = ["KEY_UP",
                                    "KEY_DOWN",
                                    "KEY_PPAGE",
                                    "KEY_NPAGE",
                                    "\\n"]
        self.select_options_print = {
            "Flèche haut/bas": "\nMonter/Descendre la sélection\n"
        }
        self.u_input = ""
        self.selected_row = 0
        self.selected_id = None

    def enter(self):
        """Enters the state"""
        self.u_input = ""
        super().enter()
        self.filtered_pd_df = self.pandas_df
        self.lims = [0, self.itf.console.height - 10]

    def exit(self):
        super().exit()

    def reset(self):
        """Resets the class"""
        self.selected_row = 0
        self.selected_id = None
        self.u_input = ""

    def get_body(self):
        pandas_df = self.format_df(self.filtered_pd_df)
        cropped_pd_df = self.rebuffer_pd_df(pandas_df)
        rich_table = self.itf.pandas_to_rich(cropped_pd_df,
                                             self.rich_name,
                                             highlight=self.selected_row,
                                             row_offset=self.lims[0])
        return Body(self.itf, rich_table, title="Sélection d'une entrée")

    def get_options_grid(self):
        text_style = self.itf.console.get_style("content")
        options_grid = Table.grid(expand=True)
        options_grid.add_column(justify="left")
        options_grid.add_row(Align(Text("Sélection\n"), align="center"))
        for i in range(0, len(self.select_options_print)):
            key_print = list(self.select_options_print.keys())[i]
            option = self.select_options_print[key_print]
            text = Text("", style=text_style)
            text.append(key_print, style=text_style + Style(bold=True))
            text.append(" : " + option, style=text_style)
            options_grid.add_row(text)
        options_grid.add_row(Align(Text("Actions\n"), align="center"))
        for i in range(0, self.size()):
            key = list(self.options.keys())[i]
            key_print = self.options[key][2]
            option = self.options[key][1]
            option = re.sub("^.", "", option)
            text = Text("(", style=text_style)
            text.append(key_print, style=text_style + Style(bold=True))
            text.append(")" + option, style=text_style)
            options_grid.add_row(text)
        return options_grid

    def print_filter(self):
        last_char = Text("|",
                         style=self.itf.console.get_style("select") +
                         Style(blink=True))
        toprint = Text("Filtre : " + self.u_input) + last_char
        self.itf.layout["footer"].visible = True
        self.itf.layout["footer"].update(Footer(self.itf, toprint))
        self.display()

    def filter_table(self):
        "Gets user input to filter the table"
        self.print_filter()
        key = State.get_keypressed()
        key = re.sub("'", "", key)
        while key not in ["\\n", "\\x1b"]:
            if key == "\\x7f": 	# backspace
                try:
                    self.u_input = self.u_input[:-1]
                except IndexError:
                    self.u_input = ""
            elif unidecode(key).isalnum():
                self.u_input = self.u_input + key
            self.filtered_pd_df = self.filter_pd_df(self.pandas_df)
            self.print_filter()
            key = State.get_keypressed()
            key = re.sub("'", "", key)
        if key == "\\n":  	# Enter
            toprint = "Filtre : " + self.u_input
            self.itf.layout["footer"].update(Footer(self.itf, toprint))
            return
        if key == "\\x1b":  	# Esc
            self.u_input = ""
            self.filtered_pd_df = self.pandas_df
            self.itf.layout["footer"].visible = False
        return

    def update(self, key=None):
        key = super().update(key)
        max_rownumber = self.get_max_rownumber()
        if key == "f":
            self.filter_table()
        elif key == "KEY_DOWN":		# flèche bas
            if max_rownumber > 0:
                self.selected_row = (self.selected_row + 1) % max_rownumber
        elif key == "KEY_UP":		# flèche haut
            if max_rownumber > 0:
                self.selected_row = (self.selected_row - 1) % max_rownumber
        elif key == "KEY_NPAGE":
            if max_rownumber > 0:
                self.selected_row = min(self.selected_row + 6, max_rownumber-1)
        elif key == "KEY_PPAGE":
            if max_rownumber > 0:
                self.selected_row = max(self.selected_row - 6, 0)
        elif key in self.options.keys() and self.options[key][4]:
            try:
                self.selected_id = self.get_selected_id()
            except EmptyDataFrameError:
                self.next = None
                self.isdone = False
        return key

    def display(self):
        if self.u_input != "":
            self.itf.layout["footer"].visible = True
        super().display()

    def filter_pd_df(self, pandas_df):
        """Filters the pandas df on any column based on user input.
        Search is case and accents independant"""
        if pandas_df.empty:
            return pandas_df
        filtered_pandas_df = pandas_df[
            pandas_df.apply(
                lambda row: row.astype(str).
                str.normalize("NFKD").
                str.encode("ascii", errors="ignore").
                str.decode("utf-8").
                str.contains(unidecode(self.u_input), case=False).
                any(),
                axis=1
            )
        ]
        return filtered_pandas_df

    def set_df_lims(self):
        print_size = self.itf.console.height - 10
        if self.lims[0] <= self.selected_row <= self.lims[1]:
            return
        elif 0 <= self.selected_row < self.lims[0]:
            self.lims = [self.selected_row, self.selected_row + print_size]
        elif self.lims[1] < self.selected_row:
            self.lims = [self.selected_row - print_size, self.selected_row]
        return

    def rebuffer_pd_df(self, pandas_df):
        """Crops pandas df to fit in the layout"""
        self.set_df_lims()
        if self.get_max_rownumber() <= self.itf.console.height - 10:
            return pandas_df
        else:
            cropped_pd_df = pandas_df.\
                iloc[self.lims[0]:self.lims[1]+1, :]
            return cropped_pd_df

    def get_max_rownumber(self):
        """returns the maximum row number to print"""
        max_rownumber = self.filtered_pd_df.shape[0]
        return max_rownumber

    def get_selected_id(self):
        """Gets selected id in table. pandas_df must have id column"""
        if self.filtered_pd_df.empty:
            raise EmptyDataFrameError
        selected_id = self.filtered_pd_df["id"].iloc[self.selected_row]
        return int(selected_id)

    def keypress_prompt(self):
        """prompt with keypresses,
        show message in footer if choice not accepted"""
        key = State.get_keypressed()
        key = re.sub("'", "", key)
        options = list(self.options.keys())
        base_style = self.itf.console.get_style("base")
        while key not in options + self.cycle_options + self.select_options_list:
            self.itf.layout["footer"].visible = True
            self.itf.layout["footer"].update(
                Footer(self.itf,
                       f"{key} : commande non reconnue ")
            )
            self.itf.console.print(self.itf.layout, style=base_style)
            key = State.get_keypressed()
            key = re.sub("'", "", key)
        self.itf.layout["footer"].visible = False
        return key


class MultiSelectMenu(SelectMenu):
    def __init__(self, name, itf):
        super().__init__(name, itf)
        self.pandas_dict = {}
        self.panel = ""
        self.select_options_list = ["KEY_DOWN",
                                    "KEY_UP",
                                    "\\n",
                                    "\\t",
                                    "KEY_RIGHT",
                                    "KEY_LEFT"]
        self.select_options_print = {
            "Flèche gauche/droite ou Tab": "\nSélectionner\
            un tableau",
            "Flèche haut/bas": "\nMonter/Descendre la sélection\n"
        }

    def enter(self):
        super().enter()
        self.itf.layout["footer"].visible = False
        self.selected_row = 0
        self.pandas_dict = self.get_pandas_dict()
        self.panel = list(self.pandas_dict.keys())[0]
        self.pandas_df = self.pandas_dict[self.panel][0]
        self.filtered_pd_df = self.pandas_df
        self.itf.four_body()

    def exit(self):
        super().exit()
        self.itf.one_body()

    def display(self):
        if self.u_input != "":
            self.itf.layout["footer"].visible = True
        text_style = self.itf.console.get_style("content")
        base_style = self.itf.console.get_style("base")
        # Body
        self.get_body()
        # Options
        options_grid = super().get_options_grid()
        quit_text = Text("", style=text_style)
        quit_text.append("q, Esc", style=text_style + Style(bold=True))
        quit_text.append(" : Quitter ce menu", style=text_style)
        options_grid.add_row(quit_text)
        self.itf.layout["topside"].update(TopSide(itf=self.itf,
                                                  renderable=options_grid,
                                                  title="Liste des commandes"))
        self.itf.console.print(self.itf.layout, style=base_style)

    def get_panel(self, panel_name):
        """Gets rawmat table and format body depending on selected rawmat"""
        table_name = self.pandas_dict[panel_name][1]
        if panel_name == self.panel:
            pandas_df = self.format_df(self.filtered_pd_df)
            pandas_df = self.rebuffer_pd_df(pandas_df)
            table = self.itf.pandas_to_rich(pandas_df,
                                            table_name,
                                            highlight=self.selected_row)
            panel_style = self.itf.console.get_style("select_panel")
            panel = Panel(Align(table,
                                align="center"),
                          style=panel_style)
        else:
            pandas_df = self.format_df(self.pandas_dict[panel_name][0])
            table = self.itf.pandas_to_rich(pandas_df, table_name)
            panel = Body(self.itf, table)
        return panel

    def update(self, key=None):
        self.pandas_df = self.pandas_dict[self.panel][0]
        key = super().update(key)
        if key == "KEY_LEFT":
            self.selected_row = 0
            self.u_input = ""
            panels_list = list(self.pandas_dict.keys())
            current_index = panels_list.index(self.panel)
            new_index = (current_index - 1) % len(panels_list)
            self.panel = panels_list[new_index]
            self.pandas_df = self.pandas_dict[self.panel][0]
            self.filtered_pd_df = self.pandas_df
        elif key in ["KEY_RIGHT", "\\t"]:
            self.selected_row = 0
            self.u_input = ""
            panels_list = list(self.pandas_dict.keys())
            current_index = panels_list.index(self.panel)
            new_index = (current_index + 1) % len(panels_list)
            self.panel = panels_list[new_index]
            self.pandas_df = self.pandas_dict[self.panel][0]
            self.filtered_pd_df = self.pandas_df
        return key


class TreeMenu(SelectMenu):
    def __init__(self, name, itf):
        super().__init__(name, itf)
        self.select_options_print = {
            "Flèche haut/bas": "\nMonter/Descendre la pré-sélection",
            "Flèche gauche/droite": "\nMonter/Descendre dans une branche"
            # "Tab": "Réduire/Développer la branche",
            # "Maj+Tab": "Réduire/Développer toutes les branches"
        }
        self.select_options_list.append("KEY_LEFT")
        self.select_options_list.append("KEY_RIGHT")

    def enter(self):
        super().enter()

    def update(self, key=None):
        key = super().update()
        if key == "KEY_DOWN":		# flèche bas
            self.selected_id = self.selectable_ids()[self.selected_row]
        elif key == "KEY_UP":		# flèche haut
            self.selected_id = self.selectable_ids()[self.selected_row]
        elif key == "KEY_RIGHT":
            self.selected_id = self.down_branch()
        elif key == "KEY_LEFT":
            self.selected_id = self.up_branch()
        return key

    def get_max_rownumber(self):
        """returns max row number selectable"""
        ids = self.selectable_ids()
        maxrows = len(ids)
        return maxrows

    def down_branch(self):
        """Selects down one branch"""

    def up_branch(self):
        """Selectes up one branch"""

    def selectable_ids(self):
        """Returns list of selectable ids"""
        return []

    def get_selected_id(self):
        if not self.pandas_df.empty:
            return self.selectable_ids()[self.selected_row]

    def generate_tree(self, pandas_df, tree, highlight):
        """Recursively generates recipes tree"""
