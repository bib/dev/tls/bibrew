"""Recipes menus definitions"""
from lib.rich.rich.tree import Tree
from lib.rich.rich.prompt import Prompt, FloatPrompt

from base_menu import BaseMenu, SelectMenu
# from flask_app.schema import (
#     Recipe,
#     RecipeWaterInput,
#     RecipeGrainInput,
#     RecipeHopInput,
#     RecipeMiscInput
# )

class RecipesMenu(BaseMenu):
    def __init__(self, name, itf):
        super().__init__(name, itf)
        self.generate_body()
        self.options = {
            "v" : ["view_recipe", "Voir"],
            "c" : ["create_recipe", "Créer"],
            "m" : ["update_recipe", "Modifier"],
            "s" : ["remove_recipe", "Supprimer"],
            "i" : ["import_recipe", "Importer"],
            "r" : ["do_recipe", "Réaliser"]
        }

    def generate_body(self):
        """Makes the body"""
        self.body = Body(self.itf, "", "Menu des recettes")

class SelectRecipe(SelectMenu):
    def generate_stmt(self):
        """Generates the body"""
        stmt = select(Recipe.id,
                      Recipe.name,
                      Recipe.parent,
                      Recipe.description,
                      Recipe.volume).\
                      where(Recipe.name.ilike('%'+self.u_input+'%')).all()
        self.stmt = stmt

    def generate_tree(self):
        """Generates recipes tree"""
        
        
class ViewRecipe(BaseMenu):
    def display(self):
        self.itf.layout["topside"].visible = False
        self.itf.layout["footer"].visible = True
        self.itf.layout["footer"].update("Appuyer sur n'importe quelle touche permet de revenir au menu des recettes")
        self.generate_body()
        self.itf.console.print(self.itf.layout)

    def update(self):
        State.get_keypressed()
        self.isdone = True

    def generate_body(self):
        grain_richtable
