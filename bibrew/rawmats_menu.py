"""Rawmats database menus definitions"""
import pandas as pd
import json

from lib.rich.rich.console import CancelByUser

from bibrew.panels import Body
from bibrew.base_menus import BaseMenu, SelectMenu, MultiSelectMenu


class RawmatsMenu(MultiSelectMenu):
    def __init__(self, name, itf):
        super().__init__(name, itf)
        self.rawmat = "g"
        self.add_option("\\n",
                        "rawmat_menu",
                        "  : Afficher le tableau sélectionné",
                        "Entrée",
                        select_id=True)
        self.add_option("a",
                        "add_rawmat",
                        "Ajouter une matière première",
                        "a")

    def get_rawmat_df(self, rawmat):
        json_nb = self.itf.sess.get(
            self.itf.url +
            f"{self.itf.rawmats_dict[rawmat][1]}s_count"
        ).json()
        nb_lines = json_nb["nb"]
        pandas_df = pd.DataFrame({"Nombre d'entrées": [nb_lines]})
        return pandas_df

    def get_pandas_dict(self):
        """Return dictionnary of pandas dataframe"""
        grains_df = self.get_rawmat_df("g")
        hops_df = self.get_rawmat_df("h")
        yeasts_df = self.get_rawmat_df("y")
        miscs_df = self.get_rawmat_df("m")
        pandas_dict = {
            "g": [grains_df, "Grains"],
            "h": [hops_df, "Houblons"],
            "y": [yeasts_df, "Levures"],
            "m": [miscs_df, "Autres"]
        }
        return pandas_dict

    def get_body(self):
        grains_panel = self.get_panel("g")
        hops_panel = self.get_panel("h")
        yeasts_panel = self.get_panel("y")
        miscs_panel = self.get_panel("m")

        self.itf.layout["body_topleft"].update(grains_panel)
        self.itf.layout["body_midleft"].update(yeasts_panel)
        self.itf.layout["body_topright"].update(hops_panel)
        self.itf.layout["body_midright"].update(miscs_panel)

    def get_selected_id(self):
        return

    def exit(self):
        super().exit()
        self.rawmat = self.panel


class SelectRawmat(SelectMenu):
    def __init__(self, name, itf):
        super().__init__(name, itf)
        self.rawmat = None

    def enter(self):
        self.rawmat = self.previous.rawmat
        super().enter()

    def get_pandas_df(self, rawmat=None):
        if rawmat is None:
            rawmat = self.rawmat
        json = self.itf.sess.get(self.itf.url +
                                 self.itf.rawmats_dict[rawmat][1] + "s").json()
        pandas_df = pd.DataFrame(json)
        pandas_df.drop(pandas_df.filter(regex="stocks").columns,
                       axis=1,
                       inplace=True)
        return pandas_df

    def get_body(self):
        pandas_df = self.format_df(self.filtered_pd_df)
        cropped_pd_df = self.rebuffer_pd_df(pandas_df)
        rich_table = self.itf.pandas_to_rich(cropped_pd_df,
                                             self.rich_name,
                                             highlight=self.selected_row,
                                             row_offset=self.lims[0])
        return Body(self.itf, rich_table, title="Sélection d'une entrée")

    def get_rich_name(self):
        """Generate rich table name"""
        if self.rawmat is None:
            return ""
        return SelectRawmat.rawmats[self.rawmat][0]

    def update(self):
        key = super().update()
        if key in list(self.options.keys()) and key != "f":
            self.parent.pop_state()
        return key


class RemoveRawmat(BaseMenu):
    def enter(self):
        super().enter()
        self.previous.enter()
        self.itf.layout["footer"].visible = True
        self.rawmat = self.previous.rawmat

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()
        self.previous.exit()
        self.previous.reset()

    def display(self):
        return

    def update(self):
        confirm = self.itf.ask_confirm(prompt="Valider la suppression ? ",
                                       default=True)
        if not confirm:
            self.isdone = True
            return
        rawmat_id = self.previous.selected_id
        self.itf.sess.delete(
            self.itf.url +
            f"{self.itf.rawmats_dict[self.rawmat][1]}/{rawmat_id}"
        )
        self.isdone = True

    def get_body(self):
        return


class AddRawmat(BaseMenu):
    def __init__(self, name, itf, default=None):
        super().__init__(name, itf)
        self.default = default

    def enter(self):
        super().enter()
        self.itf.layout["footer"].visible = True
        self.rawmat = self.previous.rawmat
        self.pandas_df = SelectRawmat.get_pandas_df(self)

    def exit(self):
        self.itf.layout["footer"].visible = False
        self.default = None
        super().exit()

    def update(self):
        self.display()
        rawmat_name = f"{self.itf.rawmats_dict[self.rawmat][1]}s"
        if self.pandas_df.empty:
            coltypes = self.itf.sess.get(
                self.itf.url + f"{rawmat_name}_types"
            ).json()
            coltypes.pop("id", None)
        else:
            coltypes = dict(self.pandas_df.copy().
                            drop("id",
                                 axis=1,
                                 errors="ignore").dtypes)
        to_add_dict = {}
        types_mapper = {
            "object": self.itf.ask_str,
            "str": self.itf.ask_str,
            "float64": self.itf.ask_float,
            "float": self.itf.ask_float,
            "int64": self.itf.ask_float,
            "int": self.itf.ask_float,
            "bool": self.itf.ask_confirm
        }
        for column in coltypes:
            if self.default is not None:
                default_col = self.default[column].values[0]
            else:
                default_col = None
            coltype = coltypes[column]
            if not isinstance(coltype, str):
                coltype = coltypes[column].name
            try:
                to_add_dict[column] = types_mapper[coltype](
                    f"{column} ",
                    default=default_col
                )
            except CancelByUser:
                self.isdone = True
                return
        self.itf.sess.post(
            self.itf.url + f"{self.itf.rawmats_dict[self.rawmat][1]}s",
            data=json.dumps(to_add_dict)
        )
        self.isdone = True


class UpdateRawmat(BaseMenu):
    def enter(self):
        super().enter()
        self.pandas_df = self.previous.pandas_df
        self.itf.layout["footer"].visible = True
        self.rawmat = self.previous.rawmat

    def exit(self):
        self.itf.layout["footer"].visible = False
        self.previous.exit()
        self.previous.reset()

    def display(self):
        return

    def update(self):
        coltypes = dict(self.pandas_df.copy().
                        drop("id", axis=1, errors="ignore").
                        dtypes)
        print(coltypes)
        rawmat_id = self.previous.selected_id
        pd_df = self.previous.pandas_df
        default_df = pd_df.loc[pd_df["id"] == rawmat_id]
        to_add_dict = {}
        for column in coltypes:
            try:
                default = default_df[column].values[0]
                print(default)
                if coltypes[column] == "object":
                    to_add_dict[column] = self.itf.ask_str(f"{column} ",
                                                           default=default)
                elif coltypes[column] == "float64":
                    to_add_dict[column] = self.itf.ask_float(f"{column} ",
                                                             default=default)
                elif coltypes[column] == "int64":
                    to_add_dict[column] = self.itf.ask_float(f"{column} ",
                                                             default=default)
                elif coltypes[column] == "bool":
                    to_add_dict[column] = self.itf.\
                        ask_confirm(f"{column} ",
                                    default=default)
            except CancelByUser:
                return
        self.itf.sess.put(self.itf.url +
                          f"{self.itf.rawmats_dict[self.rawmat][1]}/" +
                          f"{rawmat_id}",
                          data=to_add_dict)
        self.isdone = True

    def get_body(self):
        return
