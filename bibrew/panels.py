"""Base panels style"""
from datetime import datetime

from lib.rich.rich.panel import Panel
from lib.rich.rich.table import Table
from lib.rich.rich.align import Align


class Header(Panel):
    """Display header with clock."""
    def __init__(self, itf, renderable="", *args, **kwargs):
        super().__init__(renderable, *args, *kwargs)
        self.style = itf.console.get_style("header")

    def __rich__(self) -> Panel:
        grid = Table.grid(expand=True)
        grid.add_column(justify="center", ratio=1)
        grid.add_column(justify="right")
        grid.add_row(
            "BIBrew",
            datetime.now().strftime('%d %b %Y - %H:%M:%S').
            replace(":", "[blink]:[/]"),
        )
        return Panel(grid, style=self.style)


class BlinkHeader(Panel):
    """Display header with clock."""
    def __init__(self, itf, renderable="", *args, **kwargs):
        super().__init__(renderable, *args, *kwargs)
        self.style = itf.console.get_style("header")

    def __rich__(self) -> Panel:
        grid = Table.grid(expand=True)
        grid.add_column(justify="center", ratio=1)
        grid.add_column(justify="right")
        grid.add_row(
            "[blink]BIBrew[/]",
            datetime.now().
            strftime('%d %b %Y - %H:%M:%S').
            replace(":", "[blink]:[/]")
        )
        return Panel(grid, style=self.style)


class Footer(Panel):
    """Displays the footer"""
    def __init__(self,
                 itf,
                 renderable,
                 title="Invite de Commande",
                 *args,
                 **kwargs):
        super().__init__(renderable, *args, *kwargs)
        self.style = itf.console.get_style("base")
        self.title = title
        self.renderable = renderable

    def __rich__(self) -> Panel:
        return Panel(self.renderable,
                     style=self.style,
                     title=self.title)


class Body(Panel):
    """Displays the body"""
    def __init__(self, itf, renderable, title="", *args, **kwargs):
        super().__init__(renderable, *args, *kwargs)
        self.renderable = renderable
        self.title = title
        self.style = itf.console.get_style("base")

    def __rich__(self) -> Panel:
        return Panel(Align(self.renderable,
                           align="center",
                           vertical="top"),
                     title=self.title,
                     style=self.style)


class TopSide(Panel):
    """Displays the body"""
    def __init__(self, itf, renderable, title="", *args, **kwargs):
        super().__init__(renderable, *args, *kwargs)
        self.renderable = renderable
        self.title = title
        self.style = itf.console.get_style("base")

    def __rich__(self) -> Panel:
        return Panel(Align(self.renderable,
                           align="center",
                           vertical="middle"),
                     title=self.title,
                     style=self.style)
