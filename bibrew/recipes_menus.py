"""Recipes menus definitions"""
import re
import json
import pandas as pd

from datetime import date
from lib.rich.rich.tree import Tree
from lib.rich.rich.text import Text
from lib.rich.rich.align import Align
from lib.rich.rich.style import Style
from lib.rich.rich.table import Table
from lib.rich.rich.panel import Panel
from lib.rich.rich.console import CancelByUser

from bibrew.panels import Body, TopSide, Footer
from bibrew.pures import xstr
from bibrew.base_menus import (State,
                               BaseMenu,
                               MultiSelectMenu,
                               TreeMenu)
from bibrew.recipe_infos import RecipeBasicInfos, RecipeInfos


class SelectRecipe(TreeMenu):
    def __init__(self, name, itf):
        super().__init__(name, itf)
        self.selected_id = None
        self.selected_row = -1

    def down_branch(self):
        """Gets down one branch"""
        new_id = self.selected_id
        if self.selected_id is not None:
            recipe = RecipeBasicInfos(self.selected_id,
                                      self.pandas_df,
                                      self.itf)
            if recipe.has_child():
                new_id = int(recipe.get_childs().id.iloc[0])
        return new_id

    def up_branch(self):
        """Gets up one branch"""
        new_id = self.selected_id
        if self.selected_id is not None:
            recipe = RecipeBasicInfos(self.selected_id,
                                      self.pandas_df,
                                      self.itf)
            if not pd.isna(recipe.parent):
                new_id = recipe.parent
        return new_id

    def selectable_ids(self):
        """Returns list of selectable ids"""
        if self.selected_id is None:
            parent_id = None
        else:
            selected_recipe_row = self.pandas_df.\
                query(f'id == {self.selected_id}')
            parent_id = selected_recipe_row.parent.iloc[0]
        if pd.isna(parent_id):
            if self.pandas_df.empty:
                base_recipes = pd.DataFrame()
            else:
                base_recipes = self.pandas_df.query('parent.isnull()',
                                                    engine='python')
            if base_recipes.empty:
                ids = []
            else:
                ids = base_recipes.id.tolist()
            ids.append(None)
        else:
            parent_recipe_row = self.pandas_df.query(f'id == {parent_id}')
            parent_recipe = RecipeBasicInfos(parent_recipe_row.id.iloc[0],
                                             self.pandas_df,
                                             self.itf)
            ids = parent_recipe.get_childs().id.tolist()
        return ids

    def get_pandas_df(self):
        """Generates the body"""
        json = self.itf.sess.get(self.itf.url + "recipes").json()
        pandas_df = pd.DataFrame(json)
        return pandas_df

    def generate_tree(self, recipes, tree, highlight=-1):
        """Recursively generates recipes tree.
        Must give as first argument only base recipes (parent = None)"""
        if recipes.empty:
            return
        for recipe in recipes.itertuples():
            recipe_infos = RecipeBasicInfos(recipe.id,
                                            self.pandas_df,
                                            self.itf)
            if highlight == recipe.id:
                text_style = self.itf.console.get_style("select")
            else:
                text_style = self.itf.console.get_style("content")
            boldstyle = text_style + Style(bold=True)
            toprint = Text()
            toprint.append(Text(f"{recipe.name}",
                                style=boldstyle))
            toprint.append(Text(f" - {recipe.description}", style=text_style))
            if recipe_infos.has_child():
                branch = tree.add(toprint)
                childs = recipe_infos.get_childs()
                self.generate_tree(childs, branch, highlight=self.selected_id)
            else:
                tree.add(toprint)

    def get_body(self):
        """Generates the body"""
        if self.pandas_df.empty:
            base_recipes = pd.DataFrame()
        else:
            base_recipes = self.pandas_df.query('parent.isnull()',
                                                engine='python')
        tree = Tree("Liste des  recettes")
        if self.selected_id is None:
            text_style = self.itf.console.get_style("select")
        else:
            text_style = self.itf.console.get_style("content")
        tree.add(Text("Nouvelle recette", style=text_style))
        self.generate_tree(base_recipes, tree, highlight=self.selected_id)
        body = Body(self.itf,
                    Align(tree, align="left"),
                    title="Sélection d'une recette")
        return body

    def update(self):
        key = super().update()
        if key == "\\n" and self.selected_id is None:
            self.next = None
            return


class CreateRecipe(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        super().exit()
        self.itf.layout["footer"].visible = False

    def get_body(self):
        """Generates body"""
        if self.previous is not None:
            self.body = self.previous.body
        else:
            self.body = ""

    def update(self):
        try:
            name = self.itf.ask_str("Nom de la recette ")
            descri = self.itf.ask_str("Description ")
            volume = self.itf.ask_float("Volume visé ")
        except CancelByUser:
            self.isdone = True
            return
        parent = self.previous.selected_id
        self.itf.sess.post(self.itf.url + "recipes",
                           json=json.dumps({"name": name,
                                            "descri": descri,
                                            "volume": volume,
                                            "parent": parent})
                           )
        self.isdone = True


class ViewRecipe(MultiSelectMenu):
    def enter(self):
        self.recipe = RecipeInfos(self.previous.selected_id,
                                  self.previous.pandas_df,
                                  self.itf)
        self.itf.layout["topside"].visible = False
        self.itf.layout["botside"].visible = True
        self.itf.layout["body_header"].visible = True
        super().enter()
        self.itf.layout["footer"].visible = True
        self.selected_row = -1

    def exit(self):
        super().exit()
        self.itf.layout["footer"].visible = False
        self.itf.layout["topside"].visible = True
        self.itf.layout["botside"].visible = False
        self.itf.layout["body_header"].visible = False

    def display(self):
        text_style = self.itf.console.get_style("content")
        base_style = self.itf.console.get_style("base")
        # Footer
        footer_str = Text("Appuyer sur n'importe quelle touche permet de \
revenir au menu des recettes",
                          style=text_style,
                          justify="center")
        footer_panel = Panel(Align(footer_str,
                                   align="center"),
                             style=base_style)
        self.itf.layout["footer"].update(footer_panel)
        # Botside = recipes infos
        botside_panel = Panel(
            Align(self.get_stats_grid(),
                  align="center",
                  vertical="middle"),
            title="Statistiques",
            style=base_style
        )
        self.itf.layout["botside"].update(botside_panel)
        # Body head
        self.itf.layout["body_header"].update(self.get_body_head())
        # Body
        self.get_body()
        self.itf.console.print(self.itf.layout)

    def get_stats_grid(self):
        """Gets recipe stats grid"""
        text_style = self.itf.console.get_style("content")
        stats_grid = Table.grid(expand=True)
        stats_grid.add_column(justify="left")
        stats_grid.add_column(justify="center")
        stats_grid.add_row(Text("Ratio d'empâtage ",
                                style=text_style),
                           str(round(self.recipe.ratio_empat(), 2)) + " L/kg")
        stats_grid.add_row(Text("Densité",
                                style=text_style),
                           str(round(self.recipe.dens_th(), 3)))
        stats_grid.add_row(Text("EBC",
                                style=text_style),
                           str(round(self.recipe.ebc())))
        stats_grid.add_row(Text("IBU",
                                style=text_style),
                           str(round(self.recipe.ibu())))
        stats_grid.add_row(Text("Taux d'alcool",
                                style=text_style),
                           str(round(self.recipe.alcohol_th(), 2))+"% v/v")
        price_res = self.recipe.price()
        stats_grid.add_row(Text("Prix",
                                style=text_style),
                           (str(round(price_res[1], 2)) + " €"
                            if price_res[0]
                            else ">"+str(round(price_res[1], 2))+" €"))
        return stats_grid

    def update(self):
        self.display()
        State.get_keypressed()
        self.isdone = True

    def get_grains_df(self):
        grains_df = self.recipe.pandas_dict["g"]
        if not grains_df.empty:
            grains_df = grains_df[["id",
                                   "name",
                                   "step",
                                   "duration",
                                   "quantity",
                                   "Pourcentage"]]
            grains_df = self.itf.format_duration(grains_df)
        return grains_df

    def get_water_df(self):
        water_df = self.recipe.pandas_dict["w"]
        if not water_df.empty:
            water_df = water_df[["id",
                                 "step",
                                 "volume"]]
        return water_df

    def get_hops_df(self):
        hops_df = self.recipe.pandas_dict["h"]
        if not hops_df.empty:
            hops_df = hops_df[["id",
                               "name",
                               "step",
                               "duration",
                               "quantity"]]
            hops_df = self.itf.format_duration(hops_df)
        return hops_df

    def get_yeasts_df(self):
        yeasts_df = self.recipe.pandas_dict["y"].copy()
        if not yeasts_df.empty:
            yeasts_df = yeasts_df[["id",
                                   "name",
                                   "form",
                                   "delay",
                                   "quantity"]]
            yeasts_df["delay"] = yeasts_df.delay.apply(
                lambda x: str(round(x)) + "j"
            )
        return yeasts_df

    def get_miscs_df(self):
        miscs_df = self.recipe.pandas_dict["m"]
        if not miscs_df.empty:
            miscs_df = miscs_df[["id",
                                 "name",
                                 "step",
                                 "duration",
                                 "quantity"]]
            miscs_df = self.itf.format_duration(miscs_df)
        return miscs_df

    def get_pandas_dict(self):
        """Return dictionnary of pandas dataframe"""
        water_df = self.get_water_df()
        grains_df = self.get_grains_df()
        hops_df = self.get_hops_df()
        yeasts_df = self.get_yeasts_df()
        miscs_df = self.get_miscs_df()
        pandas_dict = {
            "w": [water_df, "Eau"],
            "g": [grains_df, "Grains"],
            "h": [hops_df, "Houblons"],
            "y": [yeasts_df, "Levures"],
            "m": [miscs_df, "Autres"]
        }
        return pandas_dict

    def get_body(self):
        w_df = self.format_df(self.pandas_dict["w"][0])
        w_table = self.itf.pandas_to_rich(w_df,
                                          "Eau")
        g_df = self.format_df(self.pandas_dict["g"][0])
        g_table = self.itf.pandas_to_rich(g_df,
                                          "Grains")
        h_df = self.format_df(self.pandas_dict["h"][0])
        h_table = self.itf.pandas_to_rich(h_df,
                                          "Houblons")
        y_df = self.format_df(self.pandas_dict["y"][0])
        y_table = self.itf.pandas_to_rich(y_df,
                                          "Levures")
        self.itf.layout["body_topleft"].update(Body(self.itf,
                                                    w_table))
        self.itf.layout["body_topright"].update(Body(self.itf,
                                                     g_table))
        self.itf.layout["body_midleft"].update(Body(self.itf,
                                                    y_table))
        self.itf.layout["body_midright"].update(Body(self.itf,
                                                     h_table))
        if self.pandas_dict["m"][0].empty:
            self.itf.four_body()
        else:
            m_df = self.format_df(self.pandas_dict["m"][0])
            m_table = self.itf.pandas_to_rich(m_df,
                                              "Autres")
            self.itf.five_body()
            self.itf.layout["body_botleft"].update(Body(self.itf,
                                                        m_table))

    def get_body_head(self):
        """Gets body header : recipe name, parent, descri"""
        name = self.recipe.name
        parent_list = []
        parent_id = self.recipe.parent
        while not pd.isna(parent_id):
            parent_recipe = RecipeBasicInfos(parent_id,
                                             self.recipe.recipes,
                                             self.itf)
            parent_list.insert(0, parent_recipe.name)
            parent_id = parent_recipe.parent
        delim = "-"
        parents = delim.join(parent_list)
        descri = self.recipe.description
        volume = xstr(self.recipe.volume) + " L"
        grid = Table.grid(expand=True)
        grid.add_column(justify="left")
        grid.add_column(justify="left")
        grid.add_column(justify="center")
        grid.add_column(justify="right")
        grid.add_row(parents, name, descri, volume)
        return Panel(grid, style=self.itf.console.get_style("base"))


class RemoveRecipe(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        super().exit()
        self.itf.layout["footer"].visible = False

    def get_body(self):
        """Generates body"""
        if self.previous is not None:
            self.body = self.previous.body
        else:
            self.body = ""

    def update(self):
        confirm = self.itf.ask_confirm(prompt="Valider la suppression ? ",
                                       default=True)
        if not confirm:
            self.isdone = True
            return
        recipe_id = self.previous.selected_id
        self.itf.sess.delete(self.itf.url + f"recipe/{recipe_id}")
        self.isdone = True


class EditRecipe(BaseMenu):
    def enter(self):
        self.recipe = RecipeBasicInfos(self.previous.selected_id,
                                       self.previous.pandas_df,
                                       self.itf)
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        super().exit()
        self.itf.layout["footer"].visible = False

    def get_body(self):
        """Generates body"""
        if self.previous is not None:
            self.body = self.previous.body
        else:
            self.body = ""

    def update(self):
        try:
            name = self.itf.ask_str("Nom de la recette ",
                                    default = self.recipe.name)
            descri = self.itf.ask_str("Description ",
                                      default = self.recipe.description)
            volume = self.itf.ask_float("Volume visé ",
                                        default = self.recipe.volume)
        except CancelByUser:
            self.isdone = True
            return
        recipe_id = self.previous.selected_id
        parent = self.recipe.parent
        if pd.isnull(parent):
            parent = None
        self.itf.sess.put(self.itf.url + f"recipe/{recipe_id}",
                          json=json.dumps({"name": name,
                                           "descri": descri,
                                           "volume": volume,
                                           "parent": self.recipe.parent})
                          )
        self.isdone = True


class UpdateRecipe(MultiSelectMenu):
    rawmats = {
        "g": ["Grains", "grain"],
        "y": ["Levures", "yeast"],
        "h": ["Houblons", "hop"],
        "m": ["Autres", "misc"],
        "w": ["Eau"],
    }

    def __init__(self, name, itf):
        super().__init__(name, itf)
        self.select_options = {
            "Flèche gauche/droite ou Tab": "\nSélectionner\
            une matière première",
            "Flèche haut/bas": "\nMonter/Descendre la pré-sélection\n"
        }
        self.add_option("a", "select_rawmat_r", "Ajouter une entrée", "a")
        self.add_option("i",
                        "select_stock_rawmat_r",
                        "Ajouter une entrée depuis l'inventaire",
                        "i")
        self.add_option("s",
                        "remove_recipe_input",
                        "Supprimer l'entrée sélectionnée",
                        "s",
                        select_id=True)
        self.add_option("m",
                        "update_recipe_input",
                        "Modifier l'entrée sélectionnée",
                        "m",
                        select_id=True)

    def enter(self):
        self.recipe = RecipeInfos(self.previous.selected_id,
                                  self.previous.pandas_df,
                                  self.itf)
        super().enter()
        self.itf.five_body()
        self.itf.layout["body_header"].visible = True
        self.itf.layout["topside"].visible = True
        self.itf.layout["botside"].size = 10
        self.itf.layout["botside"].visible = True

    def exit(self):
        super().exit()
        self.itf.layout["footer"].visible = False
        self.itf.layout["body_header"].visible = False
        self.itf.layout["topside"].visible = True
        self.itf.layout["botside"].size = 0
        self.itf.layout["botside"].visible = False

    def display(self):
        base_style = self.itf.console.get_style("base")
        # Botside
        botside_panel = Panel(
            Align(ViewRecipe.get_stats_grid(self),
                  align="center",
                  vertical="middle"),
            title="Statistiques",
            style=base_style
        )
        self.itf.layout["botside"].update(botside_panel)
        # Body header
        self.itf.layout["body_header"].\
            update(ViewRecipe.get_body_head(self))
        super().display()

    def get_pandas_dict(self):
        """Return dictionnary of pandas dataframe"""
        water_df = ViewRecipe.get_water_df(self)
        grains_df = ViewRecipe.get_grains_df(self)
        hops_df = ViewRecipe.get_hops_df(self)
        yeasts_df = ViewRecipe.get_yeasts_df(self)
        miscs_df = ViewRecipe.get_miscs_df(self)
        pandas_dict = {
            "w": [water_df, "Eau"],
            "g": [grains_df, "Grains"],
            "y": [yeasts_df, "Levures"],
            "h": [hops_df, "Houblons"],
            "m": [miscs_df, "Autres"]
        }
        return pandas_dict

    def get_body(self):
        water_panel = self.get_panel("w")
        grains_panel = self.get_panel("g")
        hops_panel = self.get_panel("h")
        yeasts_panel = self.get_panel("y")
        miscs_panel = self.get_panel("m")

        self.itf.layout["body_topleft"].update(water_panel)
        self.itf.layout["body_topright"].update(grains_panel)
        self.itf.layout["body_midleft"].update(yeasts_panel)
        self.itf.layout["body_midright"].update(hops_panel)
        self.itf.layout["body_botleft"].update(miscs_panel)

    def update(self):
        key = MultiSelectMenu.update(self)
        if key in list(self.options.keys()):
            self.rawmat = self.panel
            if key in ["a", "i"]:
                if self.rawmat == "w":
                    newstate = self.parent.state_dict["add_water_input"]
                    self.next = newstate
            self.next.rawmat = self.rawmat


class AddWaterInput(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        super().exit()
        self.itf.layout["footer"].visible = False

    def generate_grid(self):
        text_style = self.itf.console.get_style("content")
        options_grid = Table.grid(expand=True)
        options_grid.add_column(justify="center")
        options_grid.add_row(Align(Text("Etape\n"), align="left"))
        for opt in ["empatage", "rinçage"]:
            key = re.sub("(^.).*", "\\1", opt)
            option = re.sub("^.", "", opt)
            text = Text("(", style=text_style)
            text.append(key, style=text_style + Style(bold=True))
            text.append(")" + option, style=text_style)
            options_grid.add_row(text)

    def update(self):
        recipe_id = self.previous.recipe.rid
        steps_df = self.itf.steps_df
        step_choices = steps_df["opt"].tolist()
        try:
            step_choice = self.itf.ask_str("Etape d'ajout ",
                                           choices=step_choices,
                                           default="empat")
            volume = self.itf.ask_float("Volume ")
        except CancelByUser:
            self.isdone = True
            return
        step = steps_df.query(f'opt == "{step_choice}"').\
            iloc[0]["step"]
        self.itf.sess.post(self.itf.url + f"recipe/{recipe_id}/waters",
                           data={"step": step,
                                 "volume": volume})
        self.isdone = True


class AddRecipeInput(BaseMenu):
    def enter(self):
        super().enter()
        self.itf.layout["footer"].visible = True

    def exit(self):
        super().exit()
        self.itf.layout["footer"].visible = False

    def update(self):
        recipe_id = self.previous.previous.recipe.rid
        rawmat = self.previous.rawmat
        rawmat_id = self.previous.selected_id
        steps_df = self.itf.steps_df
        if rawmat in ["g", "h", "m"]:
            # Step
            step_choices = steps_df["opt"].to_list()
            if rawmat == "g":
                default_step = "empat"
            elif rawmat in ["h", "m"]:
                default_step = "ebu"
            try:
                step_choice = self.itf.ask_str("Etape d'ajout ",
                                               choices=step_choices,
                                               default=default_step)
            except CancelByUser:
                self.isdone = True
                return
            step = steps_df.query(f'opt == "{step_choice}"').\
                iloc[0]["step"]
            # Duration
            if step in ["MASH", "FILTER", "RINSE", "BOIL", "WHIRLPOOL"]:
                duration_unit = "minutes"
                default_duration = 60
            else:
                duration_unit = "jours"
                default_duration = 7
            try:
                duration = self.itf.ask_int(f"Durée, en {duration_unit} ",
                                            default=default_duration)
            except CancelByUser:
                self.isdone = True
                return
            # Quantity
            try:
                quantity = self.itf.ask_float("Quantité en kg ")
            except CancelByUser:
                self.isdone = True
                return
            # Post
            self.itf.sess.post(self.itf.url +
                               f"recipe/{recipe_id}/" +
                               f"{self.itf.rawmats_dict[rawmat][1]}s",
                               data={
                                   self.itf.rawmats_dict[rawmat][1]: rawmat_id,
                                   "step": step,
                                   "duration": duration,
                                   "quantity": quantity
                               })
        elif rawmat == "y":
            try:
                form = self.itf.ask_str("Type de levure",
                                        choices=['DRY', 'LIQUID', 'HARVEST'],
                                        default="DRY").\
                                        upper()
            except CancelByUser:
                self.isdone = True
                return
            try:
                delay = self.itf.ask_int(prompt="Délai avant ajout, en jours ",
                                         default=0)
            except CancelByUser:
                self.isdone = True
                return
            self.itf.sess.post(self.itf.url + f"recipe/{recipe_id}/yeasts",
                               data={"yeast": rawmat_id,
                                     "form": form,
                                     "delay": delay})
        self.isdone = True


class RemoveRecipeInput(BaseMenu):
    def enter(self):
        self.previous.enter()
        self.itf.layout["footer"].visible = True

    def exit(self):
        self.previous.exit()
        self.itf.layout["footer"].visible = False

    def get_body(self):
        self.previous.get_body()

    def update(self):
        confirm = self.itf.ask_confirm(prompt="Valider la suppression ? ",
                                       default=True)
        if not confirm:
            self.isdone = True
            return
        recipe_id = self.previous.recipe.rid
        rawmat = self.previous.rawmat
        recipe_input_id = self.previous.selected_id
        self.itf.sess.delete(self.itf.url +
                             f"recipe/{recipe_id}/" +
                             f"{self.itf.rawmats_dict[rawmat][1]}/" +
                             f"{recipe_input_id}")
        self.isdone = True


class UpdateRecipeInput(BaseMenu):
    def enter(self):
        self.previous.enter()
        self.itf.layout["footer"].visible = True

    def exit(self):
        self.previous.exit()
        self.itf.layout["footer"].visible = False

    def update(self):
        recipe_id = self.previous.recipe.rid
        rawmat = self.rawmat
        rawmat_long = self.itf.rawmats_dict[rawmat][1]
        recipe_input_id = self.previous.selected_id
        url = self.itf.url +\
            f"recipe/{recipe_id}/" +\
            f"{rawmat_long}/{recipe_input_id}"
        row = self.itf.sess.get(url).json()
        steps_df = self.itf.steps_df
        step_choices = steps_df["opt"].to_list()
        if rawmat == "w":
            default_step = row["step"]
            default_opt = steps_df.query(f'step == "{default_step}"').\
                iloc[0]["opt"]
            default_volume = row["volume"]
            step_choice = self.itf.ask_str("Etape d'ajout ",
                                           choices=step_choices,
                                           default=default_opt)
            step = steps_df.query(f'opt == "{step_choice}"').\
                iloc[0]["step"]
            volume = self.itf.ask_float("Volume ",
                                        default=default_volume)
            self.itf.sess.put(self.itf.url +
                              f"recipe/{recipe_id}/" +
                              f"water/{recipe_input_id}",
                              data={"step": step,
                                    "volume": volume})
        elif rawmat in ["g", "h", "m"]:
            default_step = row[f"recipe_{rawmat_long}_input"][0]["step"]
            default_opt = steps_df.query(f'step == "{default_step}"').\
                iloc[0]["opt"]
            default_duration = row[f"recipe_{rawmat_long}_input"][0]["duration"]
            default_qty = row[f"recipe_{rawmat_long}_input"][0]["quantity"]
            try:
                step_choice = self.itf.ask_str("Etape d'ajout ",
                                               choices=step_choices,
                                               default=default_opt)
            except CancelByUser:
                self.isdone = True
                return
            step = steps_df.query(f'opt == "{step_choice}"').\
                iloc[0]["step"]
            # Duration
            if step in ["MASH", "FILTER", "RINSE", "BOIL", "WHIRLPOOL"]:
                duration_unit = "minutes"
            else:
                duration_unit = "jours"
            try:
                duration = self.itf.ask_int(f"Durée, en {duration_unit} ",
                                            default=default_duration)
            except CancelByUser:
                self.isdone = True
                return
            # Quantity
            try:
                quantity = self.itf.ask_float("Quantité en kg ",
                                              default=default_qty)
            except CancelByUser:
                self.isdone = True
                return
            self.itf.sess.put(self.itf.url +
                              f"recipe/{recipe_id}/" +
                              f"{rawmat_long}/{recipe_input_id}",
                              data={"step": step,
                                    "duration": duration,
                                    "quantity": quantity})
        elif rawmat == "y":
            default_delay = row["recipe_yeast_input"][0]["delay"]
            default_form = row["recipe_yeast_input"][0]["form"]
            try:
                form = self.itf.ask_str("Type de levure",
                                        choices=['DRY', 'LIQUID', 'HARVEST'],
                                        default=default_form)
            except CancelByUser:
                self.isdone = True
                return
            try:
                delay = self.itf.ask_int(prompt="Délai avant ajout, en jours ",
                                         default=default_delay)
            except CancelByUser:
                self.isdone = True
                return
            self.itf.sess.put(self.itf.url +
                              f"recipe/{recipe_id}/" +
                              f"yeast/{recipe_input_id}",
                              data={"form": form,
                                    "delay": delay})
        self.isdone = True


class SetParent(BaseMenu):
    def update(self):
        # self.itf.layout["footer"].visible = False
        recipe_id = self.previous.previous.selected_id
        recipes = self.previous.pandas_df
        recipe = RecipeBasicInfos(recipe_id, recipes, self.itf)
        parent = self.previous.selected_id
        if recipe_id == parent:
            self.itf.layout["footer"].update(
                Footer(self.itf, Align(
                    Text("Seule une divinité peut être son propre parent.",
                         style=self.itf.console.get_style("content")),
                    align="center"
                ),
                       title="Erreur")
                )
            self.itf.layout["footer"].visible = True
            self.isdone = True
            return
        self.itf.sess.put(self.itf.url + f"recipe/{recipe_id}",
                          json=json.dumps({
                              "name": recipe.name,
                              "descri": recipe.description,
                              "volume": int(recipe.volume),
                              "parent": parent
                          }))
        self.isdone = True
        self.parent.pop_state()
        self.previous.previous.reset()

    def get_body(self):
        return


class DoRecipe(MultiSelectMenu):
    def __init__(self, name, itf):
        super().__init__(name, itf)
        self.enough_stock = True
        self.options = {}
        self.select_options_list = []
        self.select_options_print = {}
        self.add_option("r", "records_menu", "Réaliser la recette", "r")

    def enter(self):
        self.recipe = RecipeInfos(self.previous.selected_id,
                                  self.previous.pandas_df,
                                  self.itf)
        super().enter()
        self.itf.layout["footer"].visible = False
        self.itf.layout["topside"].visible = True

    def exit(self):
        super().exit()
        self.itf.layout["footer"].visible = False
        self.itf.layout["topside"].visible = True

    def pandas_to_rich(self, pandas_df, rich_name, rawmat):
        """Converts pandas dataframe to rich_table
        and highlights depending on stock availability"""
        raw_colnames = list(pandas_df.columns)
        if raw_colnames == [] or pandas_df.shape[0] == 0:
            return Text(f"{rich_name} \n Rien à afficher",
                        justify="center",
                        style=self.itf.console.get_style("content"))
        colnames = map(lambda x: (self.itf.colnames_dict[x]
                                  if x in self.itf.colnames_dict
                                  else x),
                       raw_colnames)
        rich_table = Table(title=rich_name, *colnames)
        # recipe_df = self.recipe.pandas_dict[rawmat].copy()
        i = 0
        for index, row in pandas_df.iterrows():
            rich_row = []
            recipe_qty = row["quantity"]
            stock_qty = row["stocks"]
            if pd.isna(stock_qty):
                self.enough_stock = False
                style = self.itf.console.get_style("not_in_stock")
            elif stock_qty < 0.9*recipe_qty:
                self.enough_stock = False
                style = self.itf.console.get_style("not_enough_stock")
            elif 0.9*recipe_qty <= stock_qty < recipe_qty:
                self.enough_stock = False
                style = self.itf.console.get_style("almost_enough_stock")
            else:
                style = self.itf.console.get_style("content")
            for colname in raw_colnames:
                rich_row.append(
                    Text(xstr(row[colname]), style=style)
                )
            rich_table.add_row(*rich_row)
            i += 1
        return rich_table

    def get_pandas_dict(self):
        """Return dictionnary of pandas dataframe"""
        water_df = ViewRecipe.get_water_df(self)
        grains_df = self.recipe.pandas_dict["g"]
        if not grains_df.empty:
            grains_df = grains_df[["id",
                                   "name",
                                   "step",
                                   "duration",
                                   "quantity",
                                   "Pourcentage",
                                   "stocks"]]
            grains_df = self.itf.format_duration(grains_df)
        hops_df = self.recipe.pandas_dict["h"]
        if not hops_df.empty:
            hops_df = hops_df[["id",
                                   "name",
                                   "step",
                                   "duration",
                                   "quantity",
                                   "stocks"]]
            hops_df = self.itf.format_duration(hops_df)
        yeasts_df = self.recipe.pandas_dict["y"]
        if not yeasts_df.empty:
            yeasts_df = yeasts_df[["id",
                                   "name",
                                   "form",
                                   "delay",
                                   "quantity",
                                   "stocks"]]
        miscs_df = self.recipe.pandas_dict["m"]
        if not miscs_df.empty:
            miscs_df = miscs_df[["id",
                                 "name",
                                 "step",
                                 "duration",
                                 "quantity",
                                 "stocks"]]
            miscs_df = self.itf.format_duration(miscs_df)
        pandas_dict = {
            "w": [water_df, "Eau"],
            "g": [grains_df, "Grains"],
            "y": [yeasts_df, "Levures"],
            "h": [hops_df, "Houblons"],
            "m": [miscs_df, "Autres"]
        }
        return pandas_dict

    def get_panel(self, panel_name):
        pandas_df = self.format_df(self.pandas_dict[panel_name][0])
        table_name = self.pandas_dict[panel_name][1]
        table = self.pandas_to_rich(pandas_df, table_name, panel_name)
        panel = Body(self.itf, table)
        return panel

    def get_body(self):
        w_df = self.format_df(self.pandas_dict["w"][0])
        w_panel = Body(self.itf, self.itf.pandas_to_rich(w_df, "Eau"))
        g_panel = self.get_panel("g")
        h_panel = self.get_panel("h")
        y_panel = self.get_panel("y")
        self.itf.layout["body_topleft"].update(w_panel)
        self.itf.layout["body_topright"].update(g_panel)
        self.itf.layout["body_midleft"].update(y_panel)
        self.itf.layout["body_midright"].update(h_panel)
        if self.pandas_dict["m"][0].empty:
            self.itf.four_body()
        else:
            self.itf.five_body()
            m_panel = self.get_panel("m")
            self.itf.layout["body_botleft"].update(m_panel)
        if not self.enough_stock:
            self.options = {}
            self.add_option("r",
                            None,
                            "Réaliser la recette telle quelle \n\
(passe à 0 dans l'inventaire si insuffisant)",
                            "r")
            self.add_option("i",
                            "stocks_menu",
                            "Inventaire : \n\
Ajouter/Modifier les stocks",
                            "i")
            self.add_option("m",
                            "update_recipe",
                            "Modifier la recette",
                            "m")

    def display(self):
        if self.u_input != "":
            self.itf.layout["footer"].visible = True
        text_style = self.itf.console.get_style("content")
        base_style = self.itf.console.get_style("base")
        # Body
        self.get_body()
        self.itf.console.print(self.itf.layout)
        # Options
        options_grid = BaseMenu.get_options_grid(self)
        quit_text = Text("", style=text_style)
        quit_text.append("q, Esc", style=text_style + Style(bold=True))
        quit_text.append(" : Quitter ce menu", style=text_style)
        options_grid.add_row(quit_text)
        self.itf.layout["topside"].update(TopSide(itf=self.itf,
                                                  renderable=options_grid,
                                                  title="Liste des commandes"))
        self.itf.console.print(self.itf.layout, style=base_style)

    def update(self):
        key = super().update()
        if key == "r":
            self.itf.layout["footer"].visible = True
            try:
                brew_date = self.itf.ask_str(
                    "Date (AAAA-MM-JJ)",
                    default=date.today().strftime("%Y-%m-%d")
                )
            except CancelByUser:
                self.isdone = True
                return
            try:
                volume = self.itf.ask_float("Volume réel", default=170)
            except CancelByUser:
                self.isdone = True
                return
            try:
                density = self.itf.ask_float("Densité initiale", default=1050)
            except CancelByUser:
                self.isdone = True
                return
            if density < 2:
                density = density*1000
            self.itf.sess.post(self.itf.url + "records",
                               json=json.dumps(
                                   {"recipe_id": self.recipe.rid,
                                    "date": brew_date,
                                    "volume": volume,
                                    "initial_density": density}
                               ))
            for rawmat in ["g", "h", "y", "m"]:
                self.deduct_quantities(rawmat)
            self.isdone = True

    def deduct_quantities(self, rawmat):
        """Update stock quantities by substracting those from the recipe"""
        recipe_df = self.recipe.pandas_dict[rawmat]
        if recipe_df.empty:
            return
        rawmat_long = self.itf.rawmats_dict[rawmat][1]
        if rawmat == "y":
            recipe_df = recipe_df.\
                groupby(['name',
                         "form",
                         "stocks",
                         rawmat_long+"s"])["quantity"].\
                sum().reset_index()
        else:
            recipe_df = recipe_df.\
                groupby(['name',
                         "stocks",
                         rawmat_long+"s"])["quantity"].\
                sum().reset_index()
        for index, row in recipe_df.iterrows():
            new_quantity = round(row["stocks"] - row["quantity"], 3)
            new_quantity = max(new_quantity, 0)
            rawmat_id = row[f"{rawmat_long}s"]
            put_data = {"quantity": new_quantity}
            if rawmat == "y":
                put_data["form"] = row["form"]
                self.itf.sess.put(self.itf.url +
                                  f"{rawmat_long}_stock/{rawmat_id}",
                                  data=put_data)
