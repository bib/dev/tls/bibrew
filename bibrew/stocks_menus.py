"""Stocks menus definitions"""
# TODO : revoir stock_menu : adapter pour class multiSelectMenu
import pandas as pd

from lib.rich.rich.console import CancelByUser

# from flask_app.schema import (
#     GrainStock,
#     HopStock,
#     YeastStock, ytypes_dict, yforms_dict, floc_dict,
#     MiscStock
# )
from bibrew.panels import Body
from bibrew.base_menus import BaseMenu, SelectMenu, MultiSelectMenu
from bibrew.rawmats_menu import SelectRawmat


class StocksMenu(MultiSelectMenu):
    def __init__(self, name, itf):
        super().__init__(name, itf)
        self.rawmat = "g"
        self.add_option("\\n",
                        "stock_details",
                        " \n  Détails de la matière première",
                        "Entrée",
                        select_id=True)
        self.add_option("a",
                        "select_rawmat_s",
                        "Ajouter une matière première",
                        "a")
        self.add_option("s",
                        "remove_stock_rawmat",
                        "Supprimer la sélection",
                        "s",
                        select_id=True)
        self.add_option("m",
                        "update_stock_rawmat",
                        "Modifier la quantité de la sélection",
                        "m",
                        select_id=True)

    def exit(self):
        super().exit()
        self.itf.layout["body_topleft"].update(Body(self.itf, ""))
        self.itf.layout["body_topright"].update(Body(self.itf, ""))
        self.itf.layout["body_midleft"].update(Body(self.itf, ""))
        self.itf.layout["body_midright"].update(Body(self.itf, ""))
        self.rawmat = self.panel

    def get_rawmat_df(self, rawmat):
        nested_json = self.itf.sess.get(
            self.itf.url +
            self.itf.rawmats_dict[rawmat][2]
        ).json()
        pandas_df = pd.json_normalize(nested_json,
                                      self.itf.rawmats_dict[rawmat][2],
                                      ["name", "id"])
        if not pandas_df.empty:
            if rawmat == "y":
                pandas_df = pandas_df[['id', 'name', 'form', 'quantity']]
            else:
                pandas_df = pandas_df[['id', 'name', 'quantity']]
        return pandas_df

    def get_pandas_dict(self):
        """Return dictionnary of pandas dataframe"""
        grains_df = self.get_rawmat_df("g")
        hops_df = self.get_rawmat_df("h")
        yeasts_df = self.get_rawmat_df("y")
        miscs_df = self.get_rawmat_df("m")
        pandas_dict = {
            "g": [grains_df, "Grains"],
            "h": [hops_df, "Houblons"],
            "y": [yeasts_df, "Levures"],
            "m": [miscs_df, "Autres"]
        }
        return pandas_dict

    def get_body(self):
        grains_panel = self.get_panel("g")
        hops_panel = self.get_panel("h")
        yeasts_panel = self.get_panel("y")
        miscs_panel = self.get_panel("m")

        self.itf.layout["body_topleft"].update(grains_panel)
        self.itf.layout["body_midleft"].update(yeasts_panel)
        self.itf.layout["body_topright"].update(hops_panel)
        self.itf.layout["body_midright"].update(miscs_panel)


class StockDetailMenu(SelectMenu):
    def __init__(self, name, itf):
        super().__init__(name, itf)
        self.add_option("a", "select_rawmat_s", "ajouter", "a")
        self.add_option("s", "remove_stock_rawmat", "supprimer", "s",
                        select_id=True)
        self.add_option("m", "update_stock_rawmat", "modifier", "m",
                        select_id=True)

    def enter(self):
        self.rawmat = self.previous.rawmat
        super().enter()

    def get_pandas_df(self):
        rawmat_json = self.itf.sess.get(
            self.itf.url +
            self.itf.rawmats_dict[self.rawmat][1]+"s"
        ).json()
        rawmat_df = pd.DataFrame(rawmat_json)
        stocks_json = self.itf.sess.get(
            self.itf.url +
            self.itf.rawmats_dict[self.rawmat][2]
        ).json()
        stocks_df = pd.json_normalize(stocks_json,
                                      self.itf.rawmats_dict[self.rawmat][2],
                                      ["id"])
        if not stocks_df.empty:
            if self.rawmat == "y":
                stocks_df = stocks_df[['id', 'form', 'quantity']]
            else:
                stocks_df = stocks_df[['id', 'quantity']]
        else:
            stocks_df = pd.DataFrame(columns=["id"])
        pandas_df = pd.merge(stocks_df, rawmat_df,
                             on="id",
                             how="inner")
        temp_cols = pandas_df.columns.tolist()
        if not pandas_df.empty:
            temp_cols.remove("name")
            temp_cols.remove("quantity")
            temp_cols.remove("price")
            new_cols = ["name"] + temp_cols + ["price", "quantity"]
            pandas_df = pandas_df[new_cols]
        return pandas_df

    def get_body(self):
        pandas_df = self.filtered_pd_df.copy().\
            drop(["id"],
                 axis=1,
                 errors="ignore")
        cropped_pd_df = self.rebuffer_pd_df(pandas_df)
        rich_table = self.itf.pandas_to_rich(cropped_pd_df,
                                             self.rich_name,
                                             highlight=self.selected_row)
        return Body(self.itf, rich_table, title="Sélection d'une entrée")


class SelectStockRawmat(SelectRawmat):
    def get_pandas_df(self):
        if self.rawmat is None:
            return pd.DataFrame()
        pandas_df = StockDetailMenu.get_pandas_df(self)
        return pandas_df


class AddRawmatQuantity(BaseMenu):
    def enter(self):
        self.rawmat = self.previous.rawmat
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        try:
            quantity = self.itf.ask_float("Quantité en kg ")
        except CancelByUser:
            self.isdone = True
            return
        rawmat_id = self.previous.selected_id
        if self.rawmat == "y":
            form = self.itf.ask_str(prompt="Type de levure",
                                    choices=['DRY', 'LIQUID', 'HARVEST'],
                                    default="DRY")
            self.itf.sess.post(self.itf.url + f"yeast_stock/{rawmat_id}",
                               data={"quantity": quantity,
                                     "form": form})
        elif self.rawmat == "g":
            self.itf.sess.post(self.itf.url + f"grain_stock/{rawmat_id}",
                               data={"quantity": quantity})
        elif self.rawmat == "h":
            self.itf.sess.post(self.itf.url + f"hop_stock/{rawmat_id}",
                               data={"quantity": quantity})
        elif self.rawmat == "m":
            self.itf.sess.post(self.itf.url + f"misc_stock/{rawmat_id}",
                               data={"quantity": quantity})
        self.isdone = True


class RemoveStockRawmat(BaseMenu):
    def enter(self):
        super().enter()
        self.previous.enter()
        self.itf.layout["footer"].visible = True
        self.rawmat = self.previous.rawmat

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()
        self.previous.exit()
        self.previous.reset()

    def display(self):
        return

    def update(self):
        confirm = self.itf.ask_confirm(prompt="Valider la suppression ? ",
                                       default=True)
        if not confirm:
            self.isdone = True
            return
        rawmat_id = self.previous.selected_id
        if self.rawmat == "g":
            self.itf.sess.delete(self.itf.url + f"grain_stock/{rawmat_id}")
        elif self.rawmat == "h":
            self.itf.sess.delete(self.itf.url + f"hop_stock/{rawmat_id}")
        elif self.rawmat == "y":
            self.itf.sess.delete(self.itf.url + f"yeast_stock/{rawmat_id}")
        elif self.rawmat == "m":
            self.itf.sess.delete(self.itf.url + f"misc_stock/{rawmat_id}")
        self.isdone = True

    def get_body(self):
        return


class UpdateStockRawmat(BaseMenu):
    def enter(self):
        super().enter()
        self.previous.enter()
        self.rawmat = self.previous.rawmat
        self.itf.layout["footer"].visible = True

    def exit(self):
        self.itf.layout["footer"].visible = False
        self.rawmat = None
        super().exit()
        self.previous.exit()
        self.previous.reset()

    def get_body(self):
        return

    def update(self):
        try:
            quantity = self.itf.ask_float("Quantité en kg ")
        except CancelByUser:
            self.isdone = True
            return
        rawmat_id = self.previous.selected_id
        if self.rawmat == "g":
            self.itf.sess.put(self.itf.url + f"grain_stock/{rawmat_id}",
                              data={"quantity": quantity})
        elif self.rawmat == "h":
            self.itf.sess.put(self.itf.url + f"hop_stock/{rawmat_id}",
                              data={"quantity": quantity})
        elif self.rawmat == "y":
            form = self.itf.ask_str(prompt="Type de levure",
                                    choices=['DRY', 'LIQUID', 'HARVEST'],
                                    default="DRY")
            self.itf.sess.put(self.itf.url + f"yeast_stock/{rawmat_id}",
                              data={"quantity": quantity,
                                    "form": form})
        elif self.rawmat == "m":
            self.itf.sess.put(self.itf.url + f"misc_stock/{rawmat_id}",
                              data={"quantity": quantity})
        self.isdone = True
