"""RecipeInfos class definition"""
import pandas as pd
import numpy as np

from bibrew.pures import (
    plato_to_dens,
    calculate_alcohol,
    pitch_rate_calc,
    yeast_quantity
)


class NoGrain(Exception):
    pass


class NoYeast(Exception):
    pass


class NoHop(Exception):
    pass


class NoData(Exception):
    pass


class RecipeBasicInfos:
    def __init__(self, recipe_id, recipes, itf):
        self.itf = itf
        self.recipes = recipes
        self.rid = recipe_id
        if self.rid is not None:
            recipe = self.recipes.query(f'id == {self.rid}')
            self.name = recipe.name.iloc[0]
            self.parent = recipe.parent.iloc[0]
            self.description = recipe.description.iloc[0]
            self.volume = recipe.volume.iloc[0]

    def has_child(self):
        """Returns true if the recipe has childs"""
        return any(self.recipes.parent.isin([self.rid]))

    def get_childs(self):
        """Returns recipe childs"""
        childs = self.recipes.query(f'parent == {self.rid}')
        return childs


class RecipeInfos(RecipeBasicInfos):
    def __init__(self, recipe_id, recipes, itf):
        super().__init__(recipe_id, recipes, itf)
        self.pandas_dict = {}
        self.pandas_dict["w"] = self.get_water_df()
        self.pandas_dict["g"] = self.get_grains_df()
        self.pandas_dict["h"] = self.get_hops_df()
        self.pandas_dict["y"] = self.get_yeasts_df()
        self.pandas_dict["m"] = self.get_miscs_df()

    def get_water_df(self):
        """Returns water pandas df"""
        water_json = self.itf.sess.get(
            self.itf.url + f"recipe/{self.rid}/waters"
        ).json()
        water_df = pd.DataFrame(water_json)
        return water_df

    def get_grains_df(self):
        """Returns grains pandas df"""
        nested_json = self.itf.sess.get(
            self.itf.url + f"recipe/{self.rid}/grains"
        ).json()
        grains_input = pd.json_normalize(nested_json,
                                         ["recipe_grain_input"],
                                         ["EBC", "max_yield", "price", "name"])
        if grains_input.empty:
            grains_input = pd.DataFrame(columns=["name",
                                                 "max_yield",
                                                 "EBC",
                                                 "price",
                                                 "id",
                                                 "grains",
                                                 "step",
                                                 "duration",
                                                 "quantity",
                                                 "recipe"])
        grains_stock = pd.json_normalize(
            nested_json,
            ["grain_stocks"],
            []).\
            rename(columns={"quantity": "stocks"}
                   )
        if grains_stock.empty:
            grains_stock = pd.DataFrame(columns=["grains", "stocks"])
        grains_df = pd.merge(grains_input,
                             grains_stock,
                             on="grains",
                             how="outer")
        if not grains_df.empty:
            grains_df = grains_df.\
                loc[grains_df["recipe"] == self.rid]
            qty_grains = grains_df["quantity"].sum()
            if qty_grains > 0:
                grains_df = grains_df.\
                    assign(
                        Pourcentage=round(
                            grains_df['quantity']*100/qty_grains, 1
                        )
                    ).\
                    sort_values(by=['step', 'duration', 'quantity'],
                                axis=0,
                                ascending=[False, False, False])
        return grains_df

    def get_hops_df(self):
        """Returns hops pandas df"""
        nested_json = self.itf.sess.get(
            self.itf.url + f"recipe/{self.rid}/hops",
        ).json()
        hops_input = pd.json_normalize(nested_json,
                                       ["recipe_hop_input"],
                                       ["alpha", "price", "name"])
        if hops_input.empty:
            hops_input = pd.DataFrame(columns=["name",
                                               "alpha",
                                               "price",
                                               "id",
                                               "hops",
                                               "step",
                                               "duration",
                                               "quantity",
                                               "recipe"])
        hops_stock = pd.json_normalize(
            nested_json,
            ["hop_stocks"],
            []).\
            rename(columns={"quantity": "stocks"}
                   )
        if hops_stock.empty:
            hops_stock = pd.DataFrame(columns=["hops", "stocks"])
        hops_df = pd.merge(hops_input, hops_stock, on="hops", how="outer").\
            sort_values(by=['step', 'duration', 'quantity'],
                        axis=0,
                        ascending=[True, False, False])
        hops_df = hops_df.loc[hops_df["recipe"] == self.rid]
        return hops_df

    def get_yeasts_df(self):
        """Returns yeasts pandas df"""
        nested_json = self.itf.sess.get(
            self.itf.url + f"recipe/{self.rid}/yeasts"
        ).json()
        yeasts_input = pd.json_normalize(nested_json,
                                         ["recipe_yeast_input"],
                                         ["type",
                                          "attenuation",
                                          "price",
                                          "name"])
        if yeasts_input.empty:
            yeasts_input = pd.DataFrame(columns=["name",
                                                 "type",
                                                 "attenuation",
                                                 "price",
                                                 "id",
                                                 "yeasts",
                                                 "form",
                                                 "delay",
                                                 "recipe"])
        yeasts_stock = pd.json_normalize(nested_json,
                                         ["yeast_stocks"],
                                         []).rename(
                                             columns={"quantity": "stocks"}
                                         )
        if yeasts_stock.empty:
            yeasts_stock = pd.DataFrame(columns=["yeasts", "stocks"])
        yeasts_df = pd.merge(yeasts_input,
                             yeasts_stock,
                             on="yeasts",
                             how="outer")
        # Calculates pitch rate
        yeasts_df = yeasts_df.\
            loc[yeasts_df["recipe"] == self.rid]
        nb_yeasts = float(yeasts_df.shape[0])
        if nb_yeasts > 0:
            yeasts_df["quantity"] = yeasts_df.apply(
                lambda x: yeast_quantity(
                    pitch_rate_calc(self.volume,
                                    self.dens_th(),
                                    x["type"])/nb_yeasts,
                    x["form"]
                ),
                axis=1
            )
            yeasts_df = yeasts_df.\
                sort_values(by=['form', 'delay', 'quantity'],
                            axis=0,
                            ascending=[True, True, False])
        return yeasts_df

    def get_miscs_df(self):
        """Returns miscs pandas df"""
        nested_json = self.itf.sess.get(
            self.itf.url + f"recipe/{self.rid}/miscs"
        ).json()
        miscs_input = pd.json_normalize(nested_json,
                                        ["recipe_misc_input"],
                                        ["price", "name"])
        if miscs_input.empty:
            miscs_input = pd.DataFrame(columns=["name",
                                                "price",
                                                "id",
                                                "miscs",
                                                "step",
                                                "duration",
                                                "quantity",
                                                "recipe"])
        miscs_stock = pd.json_normalize(nested_json,
                                        ["misc_stocks"],
                                        []).rename(
                                            columns={"quantity": "stocks"}
                                        )
        if miscs_stock.empty:
            miscs_stock = pd.DataFrame(columns=["miscs", "stocks"])
        miscs_df = pd.merge(miscs_input,
                            miscs_stock,
                            on="miscs",
                            how="outer")
        if not miscs_df.empty:
            qty_miscs = miscs_df["quantity"].sum()
            miscs_df = miscs_df.assign(
                Pourcentage=round(miscs_df['quantity']*100/qty_miscs, 1)
            ).\
                sort_values(by=['step', 'duration', 'quantity'],
                            axis=0,
                            ascending=[True, False, False]).\
                loc[miscs_df["recipe"] == self.rid]
        return miscs_df

    def qty_grains(self):
        """Gets total grain quantity"""
        grains_df = self.pandas_dict['g']
        if grains_df.empty:
            return 0
        grains_qty = self.pandas_dict["g"]["quantity"].sum()
        return grains_qty

    def ratio_empat(self):
        """Calculates mash water/grain ratio"""
        water_df = self.pandas_dict["w"]
        if water_df.empty:
            return np.nan
        water_quantity = water_df.query('step == "MASH"',
                                        parser="pandas")["volume"].sum()
        grain_quantity = self.qty_grains()
        if grain_quantity > 0:
            ratio = water_quantity/grain_quantity
        else:
            return np.nan
        return ratio

    def dens_th(self):
        """Calculates theoretical density of the recipe"""
        grains_df = self.pandas_dict["g"]
        if grains_df.empty:
            return 1.000
        sugar_qty = (grains_df["quantity"]*grains_df["max_yield"]/100).sum()
        plato = sugar_qty*80/self.volume
        dens = plato_to_dens(plato)
        return dens

    def alcohol_th(self):
        """Calculates theoretical alcohol percentage
        from apparent yeast attenuation"""
        d_deb = self.dens_th()
        yeasts_df = self.pandas_dict["y"]
        if yeasts_df.empty:
            return 0
        attenuation = yeasts_df["attenuation"].max()
        d_fin = 1 + (d_deb-1)*(1-attenuation)
        grains_df = self.pandas_dict["g"]
        if not grains_df.empty:
            added_sugar = grains_df.\
                query("step == 'CONDITIONING'")["quantity"].\
                sum()
        else:
            added_sugar = 0
        alc_vv = calculate_alcohol(d_deb, d_fin, added_sugar)
        return alc_vv

    def ebc(self):
        """Calculates recipe EBC"""
        grains_df = self.pandas_dict["g"]
        if grains_df.empty:
            return 0
        mcu = 4.24/self.volume*(grains_df["quantity"]*grains_df["EBC"]).sum()
        ebc_fin = 2.94 * mcu ** 0.6859
        return ebc_fin

    def ibu(self):
        """Calculates recipe IBU"""
        dens = self.dens_th()
        if dens < 1.050:
            correc = 1
        else:
            correc = 1 + (dens - 1.050) / 0.2
        hops_df = self.pandas_dict["h"]
        if hops_df.empty:
            return 0
        ibu_fin = (1.65 *
                   0.125 *
                   (dens - 1) *
                   ((1 - np.exp(-0.04 * hops_df["duration"])) / 4.15) *
                   hops_df["alpha"] *
                   hops_df["quantity"] *
                   1000 *
                   1000).sum()/(
                       self.volume*correc
                   )
        return ibu_fin

    def price(self):
        """Calculate recipe price"""
        perca = 0.06*5*5 	# 60g de perca pour 15L, 5 fois par brassin
        # (Transfert fermentation, frigo, embouteillage, 2 seaux en rab)
        # 5€ le kilo
        lye = 10 	#
        gas = 20*self.volume/180 	# 2/3 de bouteille par brassin de 180L
        consumable = 2 	# Petits consommables, éponge, etc.
        co2 = 5 	# Une bouteille de 10kg = 70 fûts à la tireuse, 35€
        # 0.5/fût
        # + carbonatation : 2vol CO2 = 116g, soit 85 fûts avec 10kg.
        # On arrondit à 1€/fût, 5 fûts/brassin
        base_price = perca + lye + gas + consumable + co2
        grains_price = self.get_rawmat_price("g")
        hops_price = self.get_rawmat_price("h")
        yeasts_price = self.get_rawmat_price("y")
        miscs_price = self.get_rawmat_price("m")
        rawmat_price = (grains_price[1] +
                        hops_price[1] +
                        yeasts_price[1] +
                        miscs_price[1])
        price = base_price + rawmat_price
        if not any([grains_price[0],
                    hops_price[0],
                    yeasts_price[0],
                    miscs_price[0]]):
            return (False, price)
        return (True, price)

    def get_rawmat_price(self, rawmat):
        """calculates rawmat price for recipe"""
        rawmat_df = self.pandas_dict[rawmat]
        if rawmat_df.empty:
            return (True, 0)
        price = (rawmat_df["quantity"]*rawmat_df["price"]).sum()
        if rawmat_df["price"].isna().any():
            return (False, price)
        return (True, price)


class ArchiveRecipeInfos(RecipeInfos):
    def __init__(self, recipe_id, recipes, record_id, itf):
        self.record_id = record_id
        super().__init__(recipe_id, recipes, itf)

    def get_water_df(self):
        """Returns water pandas df"""
        water_json = self.itf.sess.get(
            self.itf.url +
            f"record/{self.record_id}/recipe/waters"
        ).json()
        water_df = pd.DataFrame(water_json)
        return water_df

    def get_grains_df(self):
        """Returns grains pandas df"""
        nested_json = self.itf.sess.get(
            self.itf.url +
            f"record/{self.record_id}/recipe/grains"
        ).json()
        grains_input = pd.json_normalize(nested_json,
                                         ["archive_grain_input"],
                                         ["EBC", "max_yield", "price", "name"])
        if grains_input.empty:
            grains_input = pd.DataFrame(columns=["name",
                                                 "max_yield",
                                                 "EBC",
                                                 "price",
                                                 "id",
                                                 "grains",
                                                 "step",
                                                 "duration",
                                                 "quantity"])
        else:
            grains_input = grains_input.\
                    loc[grains_input["recipe"] == self.rid]
            qty_grains = grains_input["quantity"].sum()
            if qty_grains > 0:
                grains_input = grains_input.\
                    assign(
                        Pourcentage=round(
                            grains_input['quantity']*100/qty_grains,
                            1
                        )
                    ).\
                    sort_values(by=['step', 'duration', 'quantity'],
                                axis=0,
                                ascending=[True, False, False])
        return grains_input

    def get_hops_df(self):
        """Returns hops pandas df"""
        nested_json = self.itf.sess.get(
            self.itf.url +
            f"record/{self.record_id}/recipe/hops"
        ).json()
        hops_input = pd.json_normalize(nested_json,
                                       ["archive_hop_input"],
                                       ["alpha", "price", "name"])
        if hops_input.empty:
            hops_input = pd.DataFrame(columns=["name",
                                               "alpha",
                                               "price",
                                               "id",
                                               "hops",
                                               "step",
                                               "duration",
                                               "quantity"])
        hops_input = hops_input.\
            loc[hops_input["recipe"] == self.rid].\
            sort_values(by=['step', 'duration', 'quantity'],
                        axis=0,
                        ascending=[True, False, False])
        return hops_input

    def get_yeasts_df(self):
        """Returns yeasts pandas df"""
        nested_json = self.itf.sess.get(
            self.itf.url +
            f"record/{self.record_id}/recipe/yeasts"
        ).json()
        yeasts_input = pd.json_normalize(nested_json,
                                         ["archive_yeast_input"],
                                         ["type",
                                          "attenuation",
                                          "price",
                                          "name"])
        if yeasts_input.empty:
            yeasts_input = pd.DataFrame(columns=["name",
                                                 "type",
                                                 "attenuation",
                                                 "price",
                                                 "id",
                                                 "yeasts",
                                                 "form",
                                                 "delay"])
        # Calculates pitch rate
        yeasts_input = yeasts_input.\
            loc[yeasts_input["recipe"] == self.rid]
        nb_yeasts = float(yeasts_input.shape[0])
        print(nb_yeasts)
        if nb_yeasts > 0:
            yeasts_input["quantity"] = yeasts_input.\
                apply(
                lambda x: yeast_quantity(
                    pitch_rate_calc(self.volume,
                                    self.dens_th(),
                                    x["type"])/nb_yeasts,
                    x["form"]
                ),
                axis=1
            )
            yeasts_input = yeasts_input.\
                sort_values(by=['form', 'delay', 'quantity'],
                            axis=0,
                            ascending=[True, True, False],
                            inplace=False)
        return yeasts_input

    def get_miscs_df(self):
        """Returns miscs pandas df"""
        nested_json = self.itf.sess.get(
            self.itf.url +
            f"record/{self.record_id}/recipe/miscs"
        ).json()
        miscs_input = pd.json_normalize(nested_json,
                                        ["archive_misc_input"],
                                        ["price", "name"])
        if miscs_input.empty:
            miscs_input = pd.DataFrame(columns=["name",
                                                "price",
                                                "id",
                                                "miscs",
                                                "step",
                                                "duration",
                                                "quantity"])
        if not miscs_input.empty:
            qty_miscs = miscs_input["quantity"].sum()
            miscs_input = miscs_input.\
                loc[miscs_input["recipe"] == self.rid].\
                assign(
                    Pourcentage=round(miscs_input['quantity']*100/qty_miscs, 1)
                ).\
                sort_values(by=['step', 'duration', 'quantity'],
                            axis=0,
                            ascending=[True, False, False])
        return miscs_input
