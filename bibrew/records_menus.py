"""Records class definition"""
import json
import pandas as pd
from datetime import date, datetime
from babel.dates import format_date

from lib.rich.rich.align import Align
from lib.rich.rich.console import CancelByUser

from bibrew.panels import Panel, Body
from bibrew.base_menus import BaseMenu, SelectMenu, MultiSelectMenu
from bibrew.recipes_menus import ViewRecipe
from bibrew.recipe_infos import RecipeBasicInfos, ArchiveRecipeInfos
from bibrew.pures import extract_date


class RecordMenu(SelectMenu):
    def __init__(self, name, itf):
        super().__init__(name, itf)
        self.rich_name = "Historique des brassins"
        self.add_option("\\n",
                        "view_recorded_recipe",
                        "  : Voir la recette réalisée",
                        "Entrée",
                        select_id=True)
        self.add_option("a",
                        "select_recipe_rec",
                        "Ajouter un brassin réalisé",
                        "a")
        self.add_option("s",
                        "remove_record",
                        "Supprimer un brassin réalisé",
                        "s",
                        select_id=True)
        self.add_option("m",
                        "update_record",
                        "Modifier une entrée",
                        "m",
                        select_id=True)
        # self.add_option("d",
        #                 "densities",
        #                 "Densités mesurées",
        #                 "d")
        self.add_option("c",
                        "conditioning_menu",
                        "Conditionnements",
                        "c",
                        select_id=True)
        self.recipes = None

    def enter(self):
        self.recipes = self.get_recorded_recipes()
        super().enter()

    def get_pandas_df(self):
        """Get pandas df"""
        if self.recipes.empty:
            self.recipes = pd.DataFrame(columns=["id",
                                                 "name",
                                                 "volume"])
        json = self.itf.sess.get(self.itf.url +
                                 "records").json()
        records_df = pd.DataFrame(json)
        if records_df.empty:
            records_df = pd.DataFrame(columns=["id",
                                               "recipe",
                                               "date",
                                               "volume",
                                               "initial_density",
                                               "final_density"])
            return records_df
        records_recipes = pd.merge(records_df,
                                   self.recipes,
                                   left_on="recipe",
                                   right_on="id",
                                   how="inner")
        records_recipes = records_recipes[["id_x",
                                           "recipe",
                                           "name",
                                           "date",
                                           "volume_x",
                                           "initial_density",
                                           "final_density"]].\
            rename(columns={"volume_x": "volume",
                            "id_x": "id"})
        if not records_recipes.empty:
            records_recipes["date"] = records_recipes["date"].apply(
                lambda x: datetime.
                strptime(x, "%Y-%m-%dT%H:%M:%S").
                strftime("%Y-%m-%d")
            )
        return records_recipes

    def format_df(self, pandas_df):
        pandas_df = super().format_df(pandas_df)
        pandas_df = pandas_df.copy().\
            drop("recipe", axis=1, errors="ignore")
        return pandas_df

    def get_recorded_recipes(self):
        json = self.itf.sess.get(self.itf.url + "record/recipes").json()
        recipes = pd.DataFrame(json)
        return recipes

    def get_selected_recipe(self):
        recipe_id = self.pandas_df.loc[
            self.pandas_df["id"] == self.selected_id, "recipe"
        ].values[0]
        recipe = RecipeBasicInfos(recipe_id, self.recipes, self.itf)
        return recipe

    def get_selected_date(self):
        brew_date = self.pandas_df.loc[
            self.pandas_df["id"] == self.selected_id, "date"
        ].values[0]
        return brew_date


class AddRecord(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        recipe_id = self.previous.selected_id
        self.parent.state_stack.remove(self.previous)
        if recipe_id is None:
            self.isdone = True
            return
        try:
            brew_date = self.itf.ask_str(
                "Date",
                default=date.today().strftime("%Y-%m-%d")
            )
        except CancelByUser:
            self.isdone = True
            return
        try:
            volume = self.itf.ask_float("Volume réel", default=170)
        except CancelByUser:
            self.isdone = True
            return
        try:
            density = self.itf.ask_float("Densité initiale", default=1050)
        except CancelByUser:
            self.isdone = True
            return
        if density < 2:
            density = density*1000
        self.itf.sess.post(self.itf.url + "records",
                           json=json.dumps(
                               {"recipe_id": recipe_id,
                                "date": brew_date,
                                "volume": volume,
                                "initial_density": density}
                           ))
        self.isdone = True
        return


class RemoveRecord(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        confirm = self.itf.ask_confirm(prompt="Valider la suppression ? ",
                                       default=True)
        if not confirm:
            self.isdone = True
            return
        record_id = self.previous.selected_id
        self.itf.sess.delete(self.itf.url +
                             f"record/{record_id}")
        self.isdone = True
        return


class UpdateRecord(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        record_id = self.previous.selected_id
        record = self.itf.sess.get(self.itf.url + f"record/{record_id}").json()
        default_date = extract_date(record["date"])
        try:
            brew_date = self.itf.ask_str(
                "Date",
                default=default_date
            )
        except CancelByUser:
            self.isdone = True
            return
        try:
            volume = self.itf.ask_float("Volume réel", default=record["volume"])
        except CancelByUser:
            self.isdone = True
            return
        try:
            initial_density = self.itf.ask_float(
                "Densité initiale",
                default=record["initial_density"])
        except CancelByUser:
            self.isdone = True
            return
        if initial_density < 2:
            initial_density = initial_density*1000
        try:
            final_density = self.itf.ask_float(
                "Densité finale",
                default=record["final_density"])
        except CancelByUser:
            self.isdone = True
            return
        if final_density < 2:
            final_density = final_density*1000
        self.itf.sess.put(self.itf.url + f"record/{record_id}",
                          json=json.dumps(
                              {"recipe_id": record["recipe"],
                               "date": brew_date,
                               "volume": volume,
                               "initial_density": initial_density,
                               "final_density": final_density}
                          ))
        self.isdone = True
        return


class ViewRecordedRecipe(ViewRecipe):
    def enter(self):
        recipe = self.previous.get_selected_recipe()
        self.recipe = ArchiveRecipeInfos(recipe.rid,
                                         self.previous.recipes,
                                         self.previous.selected_id,
                                         self.itf)
        self.itf.layout["topside"].visible = False
        self.itf.layout["botside"].visible = True
        self.itf.layout["body_header"].visible = True
        MultiSelectMenu.enter(self)
        self.itf.layout["footer"].visible = True
        self.selected_row = -1


class ConditioningMenu(MultiSelectMenu):
    def __init__(self, name, itf):
        super().__init__(name, itf)
        self.add_option("d",
                        "add_conditioning",
                        "  : Ajouter une date de conditionnement",
                        "d")
        self.add_option("c",
                        "add_conditioning_entry",
                        "  : Spécifier le nombre de contenants d'un volume" +
                        " donné à la date sélectionnée",
                        "c")
        self.add_option("s",
                        None,
                        "Supprimer une entrée",
                        "s",
                        select_id=True)
        self.add_option("m",
                        None,
                        "Modifier une entrée",
                        "m",
                        select_id=True)
        self.routes_dict = {
            "s": "remove",
            "m": "update",
            "d": "conditioning",
            "c": "conditioning_entry"
        }
        self.selected_date = None

    def enter(self):
        self.recipe = self.previous.get_selected_recipe()
        brew_date = datetime.strptime(self.previous.get_selected_date(),
                                      "%Y-%m-%d")
        brew_date = format_date(brew_date, locale="fr")
        self.rich_name = f"Conditionnement de {self.recipe.name} " +\
            f"brassée le {brew_date}, {self.recipe.volume}L"
        body_head = Panel(self.rich_name,
                          style=self.itf.console.get_style("base"))
        self.itf.layout["body_header"].visible = True
        self.itf.layout["body_header"].update(body_head)
        super().enter()
        if not self.pandas_dict["d"][0].empty:
            self.selected_date = self.pandas_dict["d"][0].\
                loc[self.selected_row, "id"].item()
        self.itf.two_body()

    def exit(self):
        super().exit()
        self.itf.layout["body_header"].visible = False

    def get_pandas_dict(self):
        """Get pandas dict"""
        dates_json = self.itf.sess.get(self.itf.url +
                                       f"record/{self.previous.selected_id}" +
                                       "/conditionings").json()
        dates_df = pd.DataFrame(dates_json)
        if not dates_df.empty:
            dates_df["date"] = dates_df["date"].apply(
                lambda x: datetime.
                strptime(x, "%Y-%m-%dT%H:%M:%S").
                strftime("%Y-%m-%d")
            )
        entries_json = self.itf.sess.get(
            self.itf.url +
            f"record/{self.previous.selected_id}" +
            "/conditioning_entries"
        ).json()
        if len(entries_json) > 0:
            entries_df = pd.json_normalize(
                entries_json,
                "conditioning_entry",
                [],
                errors="ignore"
            ).\
                rename(columns={"quantity": "number"})
        else:
            entries_df = pd.DataFrame()
        pandas_dict = {
            "d": [dates_df, "Dates de conditionnement"],
            "c": [entries_df, "Quantités conditionnées"]
        }
        return pandas_dict

    def get_body(self):
        dates_panel = self.get_panel("d")
        conditionings_panel = self.get_panel("c")
        self.itf.layout["body_topleft"].update(dates_panel)
        self.itf.layout["body_topright"].update(conditionings_panel)

    def get_panel(self, panel_name):
        """Gets rawmat table and format body depending on selected rawmat"""
        table_name = self.pandas_dict[panel_name][1]
        if panel_name == self.panel:
            pandas_df = self.format_df(self.filtered_pd_df)
            pandas_df = self.rebuffer_pd_df(pandas_df)
            table = self.itf.pandas_to_rich(pandas_df,
                                            table_name,
                                            highlight=self.selected_row)
            panel_style = self.itf.console.get_style("select_panel")
            panel = Panel(Align(table,
                                align="center"),
                          style=panel_style)
        else:
            df = self.pandas_dict[panel_name][0]
            pandas_df = self.format_df(df)
            if self.panel == "d" and not df.empty:
                sec_highlight = df.\
                    loc[df["conditionings"] == self.selected_date].\
                    index.tolist()
            elif self.panel == "c" and not df.empty and not \
                    self.pandas_dict["c"][0].empty:
                selected_id = self.get_selected_id()
                conditioning = self.pandas_dict["c"][0].\
                    loc[self.pandas_dict["c"][0]["id"] == selected_id,
                        "conditionings"].\
                    values[0]
                sec_highlight = df.\
                    loc[df["id"] == conditioning].\
                    index.tolist()
            else:
                sec_highlight = []
            table = self.itf.pandas_to_rich(pandas_df,
                                            table_name,
                                            sec_highlight=sec_highlight)
            panel = Body(self.itf, table)
        return panel

    def format_df(self, pandas_df):
        pandas_df = super().format_df(pandas_df)
        pandas_df = pandas_df.copy().\
            drop(["beer_to_ship", "conditionings"],
                 axis=1,
                 errors="ignore")
        return pandas_df

    def update(self):
        key = super().update()
        if key in ["KEY_DOWN", "KEY_UP"] and \
           not self.pandas_dict["d"][0].empty and \
           self.panel == "d":
            self.selected_date = self.pandas_dict["d"][0].\
                loc[self.selected_row, "id"].item()
        if key in ["s", "m"]:
            newstate_name = f"{self.routes_dict[key]}_" +\
                f"{self.routes_dict[self.panel]}"
            newstate = self.parent.state_dict[newstate_name]
            self.next = newstate


class AddConditioning(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        record_id = self.previous.previous.selected_id
        try:
            conditioning_date = self.itf.ask_str(
                "Date",
                default=date.today().strftime("%Y-%m-%d")
            )
        except CancelByUser:
            self.isdone = True
            return
        self.itf.sess.post(self.itf.url +
                           f"record/{record_id}/conditionings",
                           json=json.dumps(
                               {"date": conditioning_date}
                           ))
        try:
            density = self.itf.ask_float(
                "Densité finale (ESC pour ne pas donner de valeur)",
                default=1008)
        except CancelByUser:
            density = None
        if density is not None and density < 2:
            density = density*1000
        self.itf.sess.put(self.itf.url +
                          f"record/{record_id}/final_density",
                          json=json.dumps(
                              {"final_density": density}
                          ))
        self.isdone = True
        return


class RemoveConditioning(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        confirm = self.itf.ask_confirm(prompt="Valider la suppression ? ",
                                       default=True)
        if not confirm:
            self.isdone = True
            return
        conditioning_id = self.previous.selected_id
        self.itf.sess.delete(self.itf.url +
                             f"conditioning/{conditioning_id}")
        self.isdone = True
        return


class UpdateConditioning(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        conditioning_id = self.previous.selected_id
        try:
            conditioning_date = self.itf.ask_str(
                "Date",
                default=date.today().strftime("%Y-%m-%d")
            )
        except CancelByUser:
            self.isdone = True
            return
        self.itf.sess.put(self.itf.url +
                          f"conditioning/{conditioning_id}",
                          json=json.dumps(
                              {"date": conditioning_date}
                          ))
        self.isdone = True
        return


class SelectConditioning(SelectMenu):
    def get_pandas_df(self):
        record_id = self.previous.selected_id
        conditioning_json = self.itf.sess.get(
            self.itf.url + f"record/{record_id}/conditionings"
        ).json()
        conditioning_df = pd.DataFrame(conditioning_json)
        return conditioning_df

    def update(self):
        if self.pandas_df.shape[0] == 1:
            self.selected_row = 0
            self.selected_id = self.get_selected_id()
            state_name = self.options["\\n"][0]
            next_state = self.parent.state_dict[state_name]
            self.next = next_state
            return "\\n"
        else:
            key = super().update()
            return key


class SelectConditioningEntry(SelectMenu):
    def get_pandas_df(self):
        conditioning_id = self.previous.selected_id
        conditioning_entries_json = self.itf.sess.get(
            self.itf.url + f"conditioning/{conditioning_id}/" +
            "conditioning_entries"
        ).json()
        conditioning_entries_df = pd.DataFrame(conditioning_entries_json).\
            loc[:, ["id", "volume", "quantity"]]
        return conditioning_entries_df

    def update(self):
        if self.pandas_df.shape[0] == 1:
            self.selected_row = 0
            self.selected_id = self.get_selected_id()
            state_name = self.options["\\n"][0]
            next_state = self.parent.state_dict[state_name]
            self.next = next_state
            return "\\n"
        else:
            key = super().update()
            return key


class AddConditioningEntry(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        selected_date = self.previous.selected_date
        if selected_date is None:
            self.isdone = True
            return
        try:
            volume = self.itf.ask_float("Volume du contenant en litre",
                                        default="30")
        except CancelByUser:
            self.isdone = True
            return
        try:
            quantity = self.itf.ask_float("Nombre de contenants",
                                          default="5")
        except CancelByUser:
            self.isdone = True
            return
        self.itf.sess.post(self.itf.url +
                           f"conditioning/{selected_date}" +
                           f"/conditioning_entries",
                           json=json.dumps(
                               {"conditioning": selected_date,
                                "volume": volume,
                                "quantity": quantity}
                           ))
        self.isdone = True
        return


class RemoveConditioningEntry(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        confirm = self.itf.ask_confirm(prompt="Valider la suppression ? ",
                                       default=True)
        if not confirm:
            self.isdone = True
            return
        self.itf.sess.delete(
            self.itf.url +
            f"conditioning_entry/{self.previous.selected_id}"
        )
        self.isdone = True
        return


class UpdateConditioningEntry(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        selected_date = self.previous.selected_id
        if selected_date is None:
            self.isdone = True
            return
        selected_id = self.previous.selected_id
        try:
            volume = self.itf.ask_float("Volume du contenant en litre",
                                        default="30")
        except CancelByUser:
            self.isdone = True
            return
        try:
            quantity = self.itf.ask_float("Nombre de contenants",
                                          default="5")
        except CancelByUser:
            self.isdone = True
            return
        self.itf.sess.put(self.itf.url +
                          f"/conditioning_entry/{selected_id}",
                          json=json.dumps(
                              {"conditioning": selected_date,
                               "volume": volume,
                               "quantity": quantity}
                          ))
        self.isdone = True
        return
