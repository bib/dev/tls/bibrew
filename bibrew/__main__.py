"""Main script"""
import os
import argparse

# Suivi des stocks, préparation de recettes.
from bibrew.interface import Interface

parser = argparse.ArgumentParser(description="BIB's brewing tools")
parser.add_argument("--url",
                    type=str,
                    help="base url of bibrew server",
                    nargs="?")
args = parser.parse_args()


def main():
    """Main function"""
    os.system("clear")
    intf = Interface()
    while not intf.menu_manager.isdone:
        intf.menu_manager.update()
    os.system("clear")
    return 0


if __name__ == "__main__":
    exit(main())
