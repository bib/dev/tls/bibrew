"""Main menu definition"""
import json

import pandas as pd

from datetime import date

from lib.rich.rich.console import CancelByUser

from bibrew.panels import Body
from bibrew.base_menus import BaseMenu, SelectMenu, MultiSelectMenu
from bibrew.pures import extract_date


class OrderMenu(MultiSelectMenu):
    def __init__(self, name, itf):
        super().__init__(name, itf)
        self.add_option("a", None, "Ajouter une entrée", "a")
        self.add_option("s", None, "Supprimer une entrée", "s",
                        select_id=True)
        self.add_option("m", None, "Modifier une entrée", "m",
                        select_id=True)
        self.add_option("v", "validated_status", "Valider une commande", "v",
                        select_id=True)
        self.add_option("l",
                        "shipped_status",
                        "  : indiquer une commande livrée",
                        "l",
                        select_id=True)
        self.add_option("p",
                        "payed_status",
                        "  : indiquer une commande payée",
                        "p",
                        select_id=True)
        self.add_option("c", "clients_menu", "Clients", "c")
        self.selected_order = None
        self.routes_dict = {
            "s": "remove",
            "m": "update",
            "o": "order",
            "q": "beer_order",
            "b": "beer_to_ship"
        }

    def enter(self):
        super().enter()
        self.itf.three_body()
        self.selected_order = self.get_order_id()

    def get_orders_df(self):
        orders_nested = self.itf.sess.get(self.itf.url + "orders").json()
        orders_df = pd.json_normalize(orders_nested,
                                      "order",
                                      ["name"])
        if not orders_df.empty:
            orders_df = orders_df.\
                loc[:, ["id",
                        "due_date",
                        "name",
                        "validated",
                        "shipped",
                        "payed"]].\
                rename(
                    columns={"name": "client"}
                )
        return orders_df

    def get_beer_orders_df(self):
        beer_orders_json = self.itf.sess.get(self.itf.url +
                                             "orders/all_beer_orders").json()
        beer_orders_df = pd.DataFrame(beer_orders_json)
        if beer_orders_df.empty:
            return beer_orders_df
        recipes = self.itf.sess.get(self.itf.url + "recipes").json()
        recipes_df = pd.DataFrame(recipes)[["id", "name"]]
        beer_orders_df = pd.merge(beer_orders_df,
                                  recipes_df,
                                  left_on="recipe",
                                  right_on="id")
        beer_orders_df = beer_orders_df.\
            assign(
                volume=beer_orders_df["conditioning_type"].
                apply(lambda x: self.itf.cond_types[x])
            ).\
            drop("conditioning_type", axis=1).\
            rename(columns={
                "id_x": "id",
                "quantity": "number"
            }).\
            loc[:, ["id",
                    "order",
                    "recipe",
                    "name",
                    "volume",
                    "number"]]
        return beer_orders_df

    def get_beer_to_ship_df(self):
        beer_to_ship_json = self.itf.sess.get(self.itf.url +
                                              "orders/all_beers_to_ship").\
                                              json()
        beer_to_ship_df = pd.DataFrame(beer_to_ship_json)
        if beer_to_ship_df.empty:
            return beer_to_ship_df
        records_df = self.parent.state_dict["records_menu"].get_pandas_df()
        beer_to_ship_df = pd.merge(beer_to_ship_df,
                                   records_df,
                                   left_on="record",
                                   right_on="id")
        entries_json = self.itf.sess.get(self.itf.url +
                                         "records/conditionings").json()
        if len(entries_json) > 0:
            entries_df = pd.json_normalize(
                entries_json,
                "conditioning_entry",
                [],
                errors="ignore"
            ).\
                loc[:, ["id", "volume"]]
        beer_to_ship_df = pd.merge(beer_to_ship_df,
                                   entries_df,
                                   left_on="conditionned_beer",
                                   right_on="id").\
            rename(columns={"quantity": "number"})
        beer_to_ship_df = beer_to_ship_df.\
            drop("id", axis=1).\
            rename(columns={
                "id_x": "id",
                "volume_y": "volume"
            }).\
            loc[:, ["id",
                    "order",
                    "record",
                    "date",
                    "name",
                    "volume",
                    "number"]]
        return beer_to_ship_df

    def get_pandas_dict(self):
        orders_df = self.get_orders_df()
        beer_orders_df = self.get_beer_orders_df()
        beer_to_ship_df = self.get_beer_to_ship_df()
        pandas_dict = {
            "o": [orders_df, "Commandes"],
            "q": [beer_orders_df, "Contenu de la commande"],
            "b": [beer_to_ship_df, "Bières destinées à la commande"]
        }
        return pandas_dict

    def format_df(self, pandas_df):
        pandas_df = super().format_df(pandas_df)
        if "order" in pandas_df.columns.tolist():
            pandas_df = pandas_df.\
                loc[pandas_df["order"] == self.selected_order]
        pandas_df = pandas_df.\
            drop(["order", "recipe", "record"], axis=1, errors="ignore")
        return pandas_df

    def get_selected_id(self):
        if "order" in self.pandas_df.columns.tolist():
            self.filtered_pd_df = self.pandas_df.\
                loc[self.pandas_df["order"] == self.selected_order]
        return super().get_selected_id()

    def get_panel(self, panel_name):
        """Gets rawmat table and format body depending on selected panel"""
        table_name = self.pandas_dict[panel_name][1]
        if panel_name == "o" and self.panel != "o":
            orders_df = self.pandas_dict[panel_name][0]
            sec_highlight = orders_df.\
                loc[orders_df["id"] == self.selected_order].\
                index.tolist()
            orders_df = self.format_df(orders_df)
            table = self.itf.pandas_to_rich(orders_df,
                                            table_name,
                                            sec_highlight=sec_highlight)
            panel = Body(self.itf, table)
        else:
            panel = super().get_panel(panel_name)
        return panel

    def get_body(self):
        orders_panel = self.get_panel("o")
        quantities_panel = self.get_panel("q")
        brewed_panel = self.get_panel("b")
        self.itf.layout["body_topleft"].update(orders_panel)
        self.itf.layout["body_topright"].update(quantities_panel)
        self.itf.layout["body_midright"].update(brewed_panel)

    def get_order_id(self):
        if self.panel == "o" and not self.pandas_dict["o"][0].empty:
            order_id = self.pandas_dict["o"][0].\
                loc[self.selected_row, "id"].\
                item()
        else:
            order_id = self.selected_order
        return order_id

    def get_max_rownumber(self):
        """returns the maximum row number to print"""
        if self.panel != "o":
            pandas_df = self.format_df(self.filtered_pd_df)
            max_rownumber = pandas_df.shape[0]
        else:
            max_rownumber = super().get_max_rownumber()
        return max_rownumber

    def update(self):
        key = super().update()
        if key in ["KEY_DOWN", "KEY_UP"] and \
           not self.pandas_dict["o"][0].empty and \
           self.panel == "o":
            self.selected_order = self.get_order_id()
        elif key in ["s", "m"]:
            newstate_name = f"{self.routes_dict[key]}_" +\
                f"{self.routes_dict[self.panel]}"
            newstate = self.parent.state_dict[newstate_name]
            self.next = newstate
        elif key == "a":
            if self.panel == "o":
                newstate_name = "select_client_o"
            elif self.panel == "q":
                newstate_name = "select_recipe_o"
            elif self.panel == "b":
                newstate_name = "select_record_o"
            newstate = self.parent.state_dict[newstate_name]
            self.next = newstate


class AddOrder(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        client_id = self.previous.selected_id
        self.parent.state_stack.remove(self.previous)
        try:
            due_date = self.itf.ask_str(
                "Commande pour le",
                default=date.today().strftime("%Y-%m-%d")
            )
        except CancelByUser:
            self.isdone = True
            return
        self.itf.sess.post(self.itf.url + "orders",
                           json=json.dumps(
                               {"client": client_id,
                                "due_date": due_date}
                           ))
        # self.next = self.parent.state_dict["add_beer_order"]
        self.isdone = True
        return


class RemoveOrder(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        confirm = self.itf.ask_confirm(prompt="Valider la suppression ? ",
                                       default=True)
        if not confirm:
            self.isdone = True
            return
        order_id = self.previous.selected_id
        self.itf.sess.delete(self.itf.url +
                             f"order/{order_id}")
        self.isdone = True
        return


class UpdateOrder(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        order_id = self.previous.selected_id
        order = self.itf.sess.get(self.itf.url + f"order/{order_id}").json()
        default_due_date = extract_date(order["due_date"])
        try:
            due_date = self.itf.ask_str(
                "Commande pour le",
                default=default_due_date
            )
        except CancelByUser:
            self.isdone = True
            return
        self.itf.sess.put(self.itf.url + f"order/{order_id}",
                          json=json.dumps(
                              {"due_date": due_date}
                          ))
        self.isdone = True
        return


class ValidatedStatus(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        validated = self.itf.ask_confirm(
            prompt="La commande est-elle validée ? ",
            default=True
        )
        order_id = self.previous.selected_order
        self.itf.sess.put(self.itf.url +
                          f"order/{order_id}/validated_status",
                          json=json.dumps({"validated": validated}))
        self.isdone = True
        return


class ShippedStatus(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        shipped = self.itf.ask_confirm(
            prompt="La commande est-elle livrée ? ",
            default=True
        )
        order_id = self.previous.selected_order
        self.itf.sess.put(self.itf.url +
                          f"order/{order_id}/shipped_status",
                          json=json.dumps({"shipped": shipped}))
        self.isdone = True
        return


class PayedStatus(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        payed = self.itf.ask_confirm(
            prompt="La commande a-t'elle été payée ? ",
            default=True
        )
        order_id = self.previous.selected_order
        self.itf.sess.put(self.itf.url +
                          f"order/{order_id}/payed_status",
                          json=json.dumps({"payed": payed}))
        self.isdone = True
        return


class AddBeerOrder(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        self.itf.show_body_layout("body_topright")
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        order_id = self.previous.previous.selected_order
        recipe_id = self.previous.selected_id
        self.parent.state_stack.remove(self.previous)
        try:
            volume = self.itf.ask_str(
                "Volume du contenant (L)",
                choices=["30", "20", "0.75", "0.33"],
                default="30"
            )
        except CancelByUser:
            self.isdone = True
            return
        conditioning_type = [key for key, v in self.itf.cond_types.items()
                             if v == float(volume)][0]
        try:
            quantity = self.itf.ask_int(
                "Nombre à commander",
                default=3
            )
        except CancelByUser:
            self.isdone = True
            return
        self.itf.sess.post(self.itf.url + f"order/{order_id}/beer_orders",
                           json=json.dumps(
                               {"recipe": recipe_id,
                                "conditioning_type": conditioning_type,
                                "quantity": quantity}
                           ))
        self.isdone = True
        return


class RemoveBeerOrder(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        self.itf.show_body_layout("body_topright")
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        confirm = self.itf.ask_confirm(prompt="Valider la suppression ? ",
                                       default=True)
        if not confirm:
            self.isdone = True
            return
        beer_order_id = self.previous.selected_id
        self.itf.sess.delete(self.itf.url +
                             f"order/beer_order/{beer_order_id}")
        self.isdone = True
        return


class UpdateBeerOrder(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        self.itf.show_body_layout("body_topright")
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        beer_order_id = self.previous.selected_id
        beer_order = self.itf.sess.get(
            self.itf.url +
            f"order/beer_order/{beer_order_id}"
        ).json()
        default_cond_type = beer_order[0]["conditioning_type"]
        default_volume = float(self.itf.cond_types[default_cond_type])
        print(default_cond_type)
        print(default_volume)
        try:
            volume = self.itf.ask_float(
                "Volume du contenant",
                default=default_volume
            )
        except CancelByUser:
            self.isdone = True
            return
        conditioning_type = [key for key, v in self.itf.cond_types.items()
                             if v == float(volume)][0]
        default_quantity = int(beer_order[0]["quantity"])
        try:
            quantity = self.itf.ask_int(
                "Nombre à commander",
                default=default_quantity
            )
        except CancelByUser:
            self.isdone = True
            return
        self.itf.sess.put(self.itf.url + f"order/beer_order/{beer_order_id}",
                          json=json.dumps(
                              {"conditioning_type": conditioning_type,
                               "quantity": quantity}
                          ))
        self.isdone = True
        return


class AddBeerToShip(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        self.itf.show_body_layout("body_midright")
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        order_id = self.previous.previous.previous.previous.selected_order
        record_id = self.previous.previous.previous.selected_id
        conditioning_entry_id = self.previous.selected_id
        self.parent.state_stack.remove(self.previous.previous.previous)
        self.parent.state_stack.remove(self.previous.previous)
        self.parent.state_stack.remove(self.previous)
        conditioning_entry_df = self.previous.pandas_df
        volume = conditioning_entry_df.loc[
            conditioning_entry_df["id"] == conditioning_entry_id, "volume"
        ].values[0]
        print(volume)
        try:
            quantity = self.itf.ask_int(
                f"Quantité de contenants de {volume}L à commander",
                default=3
            )
        except CancelByUser:
            self.isdone = True
            return
        self.itf.sess.post(
            self.itf.url + f"order/{order_id}/beers_to_ship",
            json=json.dumps(
                {"record": record_id,
                 "conditionned_beer": conditioning_entry_id,
                 "quantity": quantity}
            ))
        self.isdone = True
        return


class RemoveBeerToShip(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        self.itf.show_body_layout("body_midright")
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        confirm = self.itf.ask_confirm(prompt="Valider la suppression ? ",
                                       default=True)
        if not confirm:
            self.isdone = True
            return
        beer_toship_id = self.previous.selected_id
        self.itf.sess.delete(self.itf.url +
                             f"order/beer_to_ship/{beer_toship_id}")
        self.isdone = True
        return


class UpdateBeerToShip(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        self.itf.show_body_layout("body_midright")
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        beer_to_ship_id = self.previous.selected_id
        beer_to_ship = self.itf.sess.get(
            self.itf.url +
            f"order/beer_to_ship/{beer_to_ship_id}"
        ).json()
        default_quantity = int(beer_to_ship[0]["quantity"])
        try:
            quantity = self.itf.ask_int(
                "Nombre à commander",
                default=default_quantity
            )
        except CancelByUser:
            self.isdone = True
            return
        self.itf.sess.put(self.itf.url +
                          f"order/beer_to_ship/{beer_to_ship_id}",
                          json=json.dumps(
                              {"quantity": quantity}
                          ))
        self.isdone = True
        return


class ClientMenu(SelectMenu):
    def __init__(self, name, itf):
        super().__init__(name, itf)
        self.add_option("a", "add_client", "Ajouter un client", "a")
        self.add_option("s", "remove_client", "Supprimer un client", "s",
                        select_id=True)
        self.add_option("m", "update_client", "Modifier un client", "m",
                        select_id=True)

    def get_pandas_df(self):
        clients_json = self.itf.sess.get(self.itf.url + "clients").json()
        clients_df = pd.DataFrame(clients_json).\
            loc[:, ["id",
                    "name",
                    "business_name",
                    "mail",
                    "tel",
                    "address"]]
        return clients_df


class AddClient(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        try:
            name = self.itf.ask_str("Nom")
        except CancelByUser:
            self.isdone = True
            return
        try:
            business_name = self.itf.ask_str("Raison Sociale")
        except CancelByUser:
            self.isdone = True
            return
        try:
            mail = self.itf.ask_str("Mail")
        except CancelByUser:
            self.isdone = True
            return
        try:
            address = self.itf.ask_str("Adresse")
        except CancelByUser:
            pass
        try:
            tel = self.itf.ask_str("Téléphone")
        except CancelByUser:
            pass
        self.itf.sess.post(self.itf.url + "clients",
                           json=json.dumps(
                               {"name": name,
                                "business_name": business_name,
                                "mail": mail,
                                "address": address,
                                "tel": tel}
                           ))
        self.isdone = True
        return


class RemoveClient(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        confirm = self.itf.ask_confirm(prompt="Valider la suppression ? ",
                                       default=True)
        if not confirm:
            self.isdone = True
            return
        client_id = self.previous.selected_id
        self.itf.sess.delete(self.itf.url +
                             f"client/{client_id}")
        self.isdone = True
        return


class UpdateClient(BaseMenu):
    def enter(self):
        self.itf.layout["footer"].visible = True
        super().enter()

    def exit(self):
        self.itf.layout["footer"].visible = False
        super().exit()

    def update(self):
        client_id = self.previous.selected_id
        client = self.itf.sess.get(self.itf.url +
                                   f"client/{client_id}").json()
        default_name = client["name"]
        try:
            name = self.itf.ask_str(
                "Nom",
                default=default_name
            )
        except CancelByUser:
            self.isdone = True
            return
        default_business_name = client["business_name"]
        try:
            business_name = self.itf.ask_str(
                "Raison sociale",
                default=default_business_name
            )
        except CancelByUser:
            self.isdone = True
            return
        default_mail = client["mail"]
        try:
            mail = self.itf.ask_str(
                "Mail",
                default=default_mail
            )
        except CancelByUser:
            self.isdone = True
            return
        default_address = client["address"]
        try:
            address = self.itf.ask_str(
                "Adresse",
                default=default_address
            )
        except CancelByUser:
            self.isdone = True
            return
        default_tel = client["tel"]
        try:
            tel = self.itf.ask_str(
                "Numéro de téléphone",
                default=default_tel
            )
        except CancelByUser:
            self.isdone = True
            return
        self.itf.sess.put(self.itf.url + f"client/{client_id}",
                          json=json.dumps(
                              {"name": name,
                               "business_name": business_name,
                               "mail": mail,
                               "address": address,
                               "tel": tel}
                          ))
        self.isdone = True
        return
