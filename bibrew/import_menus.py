"""Import and export recipe menus definitions"""
import pandas as pd
from datetime import datetime
from lxml import etree
import json
import os

from lib.rich.rich.table import Table
from lib.rich.rich.text import Text
from lib.rich.rich.panel import Panel
from lib.rich.rich.align import Align
from bibrew.panels import Body
from bibrew.base_menus import BaseMenu, SelectMenu, MultiSelectMenu
from bibrew.rawmats_menu import SelectRawmat, AddRawmat
from bibrew.recipe_infos import RecipeBasicInfos
from bibrew.pures import xstr


class SelectImportRecipe(SelectMenu):
    steps_dict = {
        "Boil": "BOIL",
        "First Wort": "BOIL",
        "Aroma": "WHIRLPOOL",
        "Dry Hop": "DRYHOP"
    }

    ytypes_dict = {
        "Ale": "ALE",
        "Lager": "LAGER",
        "Wheat": "ALE",
        "Wine": "ALE",
        "Champagne": "ALE"
    }
    yforms_dict = {
        "Dry": "DRY",
        "Liquid": "LIQUID",
        "Slant": "HARVEST",
        "Culture": "HARVEST"
    }
    floc_dict = {
        "Low": "LOW",
        "Medium": "MEDIUM",
        "High": "HIGH",
        "Very High": "HIGH"
    }

    def __init__(self, name, itf):
        super().__init__(name, itf)
        self.add_option("\\n",
                        "import_recipe",
                        " \n  Importer la recette sélectionnée",
                        "Entrée",
                        select_id=True)

    def enter(self):
        self.recipe = RecipeBasicInfos(self.previous.selected_id,
                                       self.previous.pandas_df,
                                       self.itf)
        self.list_xmls = os.listdir("recipes/import")
        super().enter()

    def get_pandas_df(self):
        pandas_df = pd.DataFrame(self.list_xmls,
                                 columns=["Fichier"])
        pandas_df["id"] = pandas_df.index
        return pandas_df

    def update(self):
        key = super().update()
        if key == "\\n":
            self.filename = self.list_xmls[self.selected_id]
            self.import_dict = self.import_recipe(self.filename)
        self.isdone = True

    def import_recipe(self, filename):
        path = "./recipes/import/" + filename
        try:
            rec_tree = etree.parse("recipes/import/" + filename)
        except OSError:
            self.isdone = True
            return
        root = rec_tree.getroot()
        for recipe in root.getchildren():
            # Basic Infos
            recipe_name = rec_tree.xpath("//RECIPE/NAME/text()")[0]
            Vf = rec_tree.xpath("//RECIPE/BATCH_SIZE/text()")[0]

            mash_time = sum(rec_tree.xpath(
                "//RECIPE/MASH/MASH_STEP/STEP_TIME/text()"
            ))
            try:
                grains_df = pd.read_xml(
                    path,
                    xpath="//RECIPE/FERMENTABLES/FERMENTABLE"
                )
            except ValueError:
                grains_df = pd.DataFrame()
            try:
                hops_df = pd.read_xml(path,
                                      xpath="//RECIPE/HOPS/HOP")
            except ValueError:
                hops_df = pd.DataFrame()
            try:
                yeasts_df = pd.read_xml(path,
                                        xpath="//RECIPE/YEASTS/YEAST")
            except ValueError:
                yeasts_df = pd.DataFrame()
            try:
                miscs_df = pd.read_xml(path,
                                       xpath="//RECIPE/MISCS/MISC")
            except ValueError:
                miscs_df = pd.DataFrame()
            recipe_dict = {
                "infos": {
                    "name": recipe_name,
                    "volume": Vf,
                    "mash_time": mash_time
                },
                "g": [grains_df, "Grains"],
                "h": [hops_df, "Hops"],
                "y": [yeasts_df, "Yeasts"],
                "m": [miscs_df, "Miscs"]
            }
            return recipe_dict


class ImportRecipe(MultiSelectMenu):
    def __init__(self, name, itf):
        super().__init__(name, itf)
        self.importable = True
        self.add_option("a",
                        "add_imported_rawmat",
                        "Ajouter la sélection à la base de données",
                        "a")
        self.add_option("s",
                        None,
                        "Supprimer la sélection de la recette à importer",
                        "s")
        self.add_option("r",
                        "select_rawmat_import",
                        "Remplacer la sélection par une entrée" +
                        " de la base de données",
                        "r")
        self.add_option("i",
                        None,
                        "Importer la recette telle qu'affichée",
                        "i")
        self.mapper_dict = {
            "name": "NAME",
            "max_yield": "YIELD",
            "EBC": "COLOR",
            "max_proportion": "MAX_IN_BATCH",
            "pH": None,
            "price": None,
            "aromatic": "AROMATIC",
            "bitter": "BITTER",
            "alpha": "ALPHA",
            "beta": "BETA",
            "producer": "LABORATORY",
            "type": "TYPE",
            "attenuation": "ATTENUATION",
            "min_temp": "MIN_TEMPERATURE",
            "max_temp": "MAX_TEMPERATURE",
            "floculation": "FLOCCULATION",
            "description": None
        }

    def enter(self):
        super().enter()
        self.rawmat = self.panel

    def get_pandas_dict(self):
        pandas_dict = {}
        for rawmat in ["g", "h", "y", "m"]:
            rawmat_bdd = SelectRawmat.get_pandas_df(self, rawmat=rawmat)
            pandas_df = self.previous.import_dict[rawmat][0]
            pandas_df["rawmat_id"] = pandas_df.apply(
                lambda row:
                None if not rawmat_bdd["name"].str.
                contains(f'^{row["NAME"]}$').
                any() else
                rawmat_bdd.loc[rawmat_bdd["name"] == row["NAME"],
                               "id"].values[0],
                axis=1)
            if not pandas_df.empty and \
               pandas_df["rawmat_id"].isnull().values.any():
                self.importable = False
            pandas_dict[rawmat] = [pandas_df,
                                   self.previous.import_dict[rawmat][1]]
        return pandas_dict

    def pandas_to_rich(self, pandas_df, rich_name, rawmat, highlight=-1):
        """Converts pandas dataframe to rich_table
        and highlights depending on presence in rawmat database"""
        raw_colnames = list(pandas_df.columns)
        if raw_colnames == [] or pandas_df.shape[0] == 0:
            return Text(f"{rich_name} \n Rien à afficher",
                        justify="center",
                        style=self.itf.console.get_style("content"))
        colnames = map(lambda x: (self.itf.colnames_dict[x]
                                  if x in self.itf.colnames_dict
                                  else x),
                       raw_colnames)
        rich_table = Table(title=rich_name, *colnames)
        i = 0
        for index, row in pandas_df.iterrows():
            rich_row = []
            if row["rawmat_id"] is None:
                style = self.itf.console.get_style("not_enough_stock")
            else:
                style = self.itf.console.get_style("content")
            if i == highlight:
                style = self.itf.console.get_style("select")
            for colname in raw_colnames:
                rich_row.append(
                    Text(xstr(row[colname]), style=style)
                )
            rich_table.add_row(*rich_row)
            i += 1
        return rich_table

    def get_panel(self, panel_name):
        table_name = self.pandas_dict[panel_name][1]
        if panel_name == self.panel:
            pandas_df = self.format_df(self.pandas_dict[panel_name][0])
            table = self.pandas_to_rich(pandas_df,
                                        table_name,
                                        panel_name,
                                        highlight=self.selected_row)
            panel_style = self.itf.console.get_style("select_panel")
            panel = Panel(Align(table,
                                align="center"),
                          style=panel_style)
        else:
            pandas_df = self.format_df(self.pandas_dict[panel_name][0])
            table = self.pandas_to_rich(pandas_df, table_name, panel_name)
            panel = Body(self.itf, table)
        return panel

    def get_body(self):
        grains_panel = self.get_panel("g")
        hops_panel = self.get_panel("h")
        yeasts_panel = self.get_panel("y")
        miscs_panel = self.get_panel("m")

        self.itf.layout["body_topleft"].update(grains_panel)
        self.itf.layout["body_midleft"].update(yeasts_panel)
        self.itf.layout["body_topright"].update(hops_panel)
        self.itf.layout["body_midright"].update(miscs_panel)

    def update(self):
        key = super().update()
        self.rawmat = self.panel
        if key == "s":
            confirm = self.itf.ask_confirm("Supprimer la ligne sélectionée ?")
            if confirm:
                self.pandas_df.drop(labels=self.selected_row,
                                    axis=0,
                                    inplace=True)
                self.pandas_dict[self.panel] = self.pandas_df
        elif key == "i":
            self.create_recipe()
            self.isdone = True

    def create_recipe(self):
        recipe_dict = self.pandas_dict
        now = datetime.now()
        date_time = now.strftime("%d/%m/%Y %H:%M:%S")
        # Création de la recette
        recipe_post = req.post(
            self.itf.url + "recipes",
            json=json.dumps(
                {"parent": self.previous.recipe.rid,
                 # Selected recipe is the parent
                 "name": self.previous.import_dict["infos"]["name"],
                 "descri": f"importé le {date_time} depuis" +
                 " {self.previous.filename}",
                 "volume": self.previous.import_dict["infos"]["volume"]}),
            auth=self.itf.auth
        )
        recipe_id = recipe_post.json()["new_id"]

        # Ajout des grains
        for i in range(recipe_dict["g"][0].shape[0]):
            if recipe_dict["g"][0].empty:
                break
            else:
                req.post(self.itf.url +
                         f"recipe/{recipe_id}/grains",
                         data={"grain": int(recipe_dict["g"][0]["rawmat_id"].
                                            loc[i]),
                               "step": "MASH",
                               "duration": self.previous.
                               import_dict["infos"]["mash_time"],
                               "quantity": float(recipe_dict["g"][0]["AMOUNT"].
                                                 loc[i])},
                         auth=self.itf.auth
                         )

        # Ajout des houblons
        for i in range(recipe_dict["h"][0].shape[0]):
            if recipe_dict["h"][0].empty:
                break
            else:
                req.post(self.itf.url +
                         f"recipe/{recipe_id}/hops",
                         data={"hop": int(recipe_dict["h"][0]["rawmat_id"].
                                          loc[i]),
                               "step": "BOIL",
                               "duration": float(recipe_dict["h"][0]["TIME"].
                                                 loc[i]),
                               "quantity": float(recipe_dict["h"][0]["AMOUNT"].
                                                 loc[i])},
                         auth=self.itf.auth
                         )

        # Ajout des levures
        for i in range(recipe_dict["y"][0].shape[0]):
            if recipe_dict["y"][0].empty:
                break
            else:
                req.post(self.itf.url +
                         f"recipe/{recipe_id}/yeasts",
                         data={"yeast": int(recipe_dict["y"][0]["rawmat_id"].
                                            loc[i]),
                               "form": "DRY",
                               "delay": 0},
                         auth=self.itf.auth
                         )
        self.isdone = True


class ReplaceRawmatImport(BaseMenu):
    def update(self):
        import_df = self.previous.previous.pandas_df
        rawmat_df = self.previous.pandas_df
        rawmat_id = self.previous.selected_id
        rawmat_name = rawmat_df.loc[rawmat_df["id"] == rawmat_id,
                                    "name"].values[0]
        import_df.loc[self.previous.previous.selected_row, "NAME"] = rawmat_name
        self.isdone = True


class AddRawmatFromImport(AddRawmat):
    def enter(self):
        super().enter()
        self.pandas_df = SelectRawmat.\
            get_pandas_df(self,
                          rawmat=self.previous.rawmat).copy().\
            drop("id", axis=1, errors="ignore")

    def update(self):
        import_df = self.previous.pandas_df.copy()
        columns = self.pandas_df.columns.tolist()
        default_dict = {}
        for column in columns:
            try:
                default_dict[column] = [import_df.
                                        loc[self.previous.selected_row,
                                            self.previous.mapper_dict[column]]]
            except KeyError:
                default_dict[column] = None
        self.default = pd.DataFrame.from_dict(default_dict)
        super().update()
