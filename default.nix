{ pkgs ? import <nixos-unstable> { } }:

pkgs.mkShell {
  nativeBuildInputs = [
    pkgs.python-language-server
    (pkgs.python3.withPackages
      (ps: with ps; [ python-language-server pandas lxml curtsies sqlalchemy alembic pylint ]))
  ];
}
